## Relatório Geral do Processo da Equipa

Na recolha de informação de todas as equipas, foi notória a falta de encaminhamento e de liderança nas primeiras semanas de trabalho. Foi também evidente, em certas situações, a falta de comunicação entre as várias equipas - "efeito silo".

Considerando que cada equipa trabalhou com frameworks e softwares nunca antes utilizados, teve de ser feito um investimento de tempo por parte de cada membro. Tempo este utilizado tanto para consolidar o processo de trabalho (entendê-lo e melhorá-lo), como para efetivamente trabalhar. Consequentemente, algumas equipas começaram o seu trabalho um pouco mais tarde do que o suposto.

Assim, uma aprendizagem inicial acerca do GitLab, Django, etc. poderia ter sido implementada.

Uma diferente causa do atraso e da demorada adaptação do grupo ao funcionamento do processo foi a reorganização da equipa no último mês. Nesta reorganização foi eliminada a equipa de Integração e foi substituído um elemento de PM. Assim, os membros da equipa da Integração apresentam poucos commits uma vez que foram colocados em novas equipas no final do processo.

Uma última falha notada no processo foi a falta de reencaminhamento de alguns issues, não sendo possível rastrear os mesmos de forma simples e rápida.

Todos os requisitos elaborados pela equipa de REQ foram  realizados de acordo com o documento "ES2021-Requisitos_v1.0" disponibilizado pelo professor e, todos os issues foram criados de acordo com a maior parte das indicações dadas no Slack.

Embora as adversidades referidas acima, a maioria dos contratempos acabaram por ser resolvidos (maioritariamente no último mês) notando-se uma melhoria exponencial no desenvolvimento da equipa em geral e no trabalho apresentado pela mesma. 

Desta forma, as diversas equipas apresentaram trabalho concluído sempre que lhes foi pedido, dentro das dealines definidas, contribuindo com o seu esforço para realizar com sucesso a entrega final. O objetivo de toda a equipa sempre foi concluir o projeto na sua integra adquirindo assim competências que serão aproveitadas num futuro próximo.
