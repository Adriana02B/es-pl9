```mermaid
flowchart LR
  REQ_EQUIPA(Pedro Henriques<br>Pedro Chaves<br>)
  REQ_DESCRICAO(Procedimento: <br>Criação e verificação de User Stories<br>Organização de requisitos)
  REQ_EXEMPLO(Exemplo: Issue #38)
  ARCH_EQUIPA(Pedro Mendes<br>Bernardo Arzileiro<br>Hugo Ribeiro)
  ARCH_DESCRICAO(Procedimento: <br>Criação a partir dos User Stories verificação de requisitos de produto<br>Organização de requisitos de produto em endpoints)
  ARCH_EXEMPLO(Exemplo: Issue #63)
  DESIGN_EQUIPA(Margarida Souto<br>Inês Galvão<br>Maria Raposo)
  DESIGN_DESCRICAO(Procedimento: <br>Desenvolvimento de código CSS e HTML,Criação de mockups)
  DESIGN_EXEMPLO(Exemplo: Issue #83)
  DEV_EQUIPA(Rodrigo Machado<br>Carlos Jordão<br>David Leitão<br>Gonçalo Folhas<br>Rui Bernardo<br>João Fernandes<br>Samuel Pires)
  DEV_DESCRICAO(Procedimento: <br>Desenvolvimento de código backend<br>Sempre que um código não passa nos testes, volta para a equipa de desenvolvimento)
  DEV_EXEMPLO(Exemplo: Issue #112)
 TEST_EQUIPA(Marta Santos<br>Francisco Faria<br>Guilherme Junqueira<br>Iago Bebiano<br>Maria Sarmento<br>Tiago Ventura)
 TEST_DESCRICAO(Procedimento: <br>Criação e testagem de cada um dos requistos<br>Nota: Testes apenas foram implementados teoricamente)
 TEST_EXEMPLO(Exemplo: Issue #134<br>)
 INT/DEPLOY_EQUIPA(Rodrigo Machado)
 INT/DEPLOY_DESCRICAO(Procedimento: <br>Faz a integração e o deploy do que foi desenvolvido até ao momento)

subgraph REQ
    direction LR
    REQ_EQUIPA
    REQ_DESCRICAO
    REQ_EXEMPLO
    click REQ_EXEMPLO "https://gitlab.com/Adriana02B/es-pl9/-/issues/38"
  end
  subgraph ARCH
    direction LR
    ARCH_EQUIPA
    ARCH_DESCRICAO
    ARCH_EXEMPLO
    click ARCH_EXEMPLO "https://gitlab.com/Adriana02B/es-pl9/-/issues/63"
  end
  subgraph DESIGN
    direction LR
    DESIGN_EQUIPA
    DESIGN_DESCRICAO
    DESIGN_EXEMPLO
    click DESIGN_EXEMPLO "https://gitlab.com/Adriana02B/es-pl9/-/issues/83"
  end
  subgraph DEV
    direction LR
    DEV_EQUIPA
    DEV_DESCRICAO
    DEV_EXEMPLO
    click DEV_EXEMPLO "https://gitlab.com/Adriana02B/es-pl9/-/issues/112"
  end
  subgraph TEST
    direction LR
    TEST_EQUIPA
    TEST_DESCRICAO
    TEST_EXEMPLO
  end
  subgraph INT/DEPLOY
    direction LR
    INT/DEPLOY_EQUIPA
    INT/DEPLOY_DESCRICAO
  end

  REQ --> ARCH
  ARCH --> DEV
  TEST_DESCRICAO -- Caso exista algum erro<br>nos testes --> DEV_DESCRICAO
  ARCH --> TEST
  ARCH --> DESIGN
  DESIGN --> INT/DEPLOY
  DEV -- Testes aceites --> INT/DEPLOY

 PM_EQUIPA(Adriana Bernardo<br>João Vaz)
 PM_DESCRICAO(Procedimento:<br>Gestão de recursos da equipa<br>Criação de documentos de organização)
 Q/A_PROCESS_EQUIPA(Eva Simões<br>Luís Santos)
 Q/A_PROCESS_DESCRICAO(Procedimento:<br>Verificação do que é produzido pelas outras equipas, ex.: documento de requisitos)

subgraph PM
direction LR
PM_EQUIPA
PM_DESCRICAO
end
subgraph Q/A_PROCESS
direction LR
Q/A_PROCESS_EQUIPA
Q/A_PROCESS_DESCRICAO

end
```


Nota - Este diagrama foi feito tendo como por base os seguintes ficheiros:<br>
Tabela de Rastreio: [PM/Rastreio_Issue.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/PM/Rastreio_Issue.md)
<br>
Ficheiro do Processo: [PM/Processo.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/PM/Processo.md)
<br>
Ficheiro de Atividade dos Membros: [PM/Atividade_membros.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/PM/Atividade_membros.md)
