# Equipa de Qualidade de Processo
  
## Equipa de Qualidade de Processo: 
- Eva Simões
- Luís Santos

## Processo:
- Verificação do que é produzido pelas outras equipas, ex.: documento de requisitos;
- Monotorização e avaliação do processo de trabalho (*workflow*).

## Organização da pasta:
Nesta pasta encontram-se os ficheiros referentes à qualidade de processo.
