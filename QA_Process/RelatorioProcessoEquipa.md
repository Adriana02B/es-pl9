## [Relatório] Apreciação do Processo por Equipas 

**[REQ]**

No que diz respeito ao desenvolvimento desta equipa, antes de serem publicados os requisitos, foi lido o documento "ES2021-Requisitos_v1.0" divulgado pelo professor tendo sido, de seguida, criados "user stories" (issue) e um conjunto de testes correspondentes para esses mesmos issues. 
Todos os elementos do grupo mostraram-se recetivos e abertos a eventuais correções das suas submissões. 
Concluindo,  a equipa de requisitos apresentou resultados satisfatórios o que permitiu que o restante processo decorresse consoante a possível normalidade fazendo assim uma avaliação positiva do mesmo.

**[ARCH]**

A equipa de Arquitetura apresentou algumas dificuldades a nível de distribuição de trabalho, nomeadamente na construção da Tabela de Requisitos de Produto sendo necessário recorrer a membros de outras equipas para a elaboração da mesma. No entanto, tudo foi entregue corretamente e de acordo com a deadline.
Esta equipa encontrou-se um pouco prejudicada tendo em conta que apenas tinha dois membros tendo sido adicionado um outro membro apenas no último mês de trabalho. Membro este que anteriormente compunha da equipa de Integração.
A equipa elaborou o diagrama de Arquitetura, de acordo com as indicações do professor, e mais tarde, juntamente com outras equipas, os requisitos de produto.

**[DEV]**

Um dos aspetos que poderia ter sido melhorado na equipa de Desenvolvimento foi a falta de check-ins frequentes, o que levou a uma insuficiência de controlo da equipa numa fase inicial. Esta pequena falha levou a que dois membros da equipa trabalhassem no mesmo requisito, tendo sido corrigido mais tarde.
A criação de um "boilerplate" por parte do team leader ajudou os restantes membros a desenvolver os requisitos de forma mais rápida e eficiente.
A dinâmica da equipa foi bastante produtiva pois, para além do referido acima, o team leader mostrou-se sempre disponível em tirar dúvidas aos membros relativamente a Django, Python e GitLab. 

**[TESTING/QA]**


- **Qualidade de Produto**

Tento em conta o curto espaço de tempo que a equipa de Testing/QA teve disponível para realizar a Tabela de Casos de Testing (última semana antes da entrega), foi notório o bom desempenho por parte da mesma. 
O resultado obtido estava de acordo com o esperado, visto que cada membro da equipa submeteu em média 3 issues.
Tendo novamente em conta as deadlines dadas a esta equipa, tornou-se difícil a implementação em código dos testes da tabela acima referida. Desta forma, foram  apenas pensados e registados de forma teórica.


- **Qualidade de Processo**

Esta equipa foi formada apenas no último mês do processo, estando anteriormente os seus membros da equipa de Testing de Produto e na equipa de Integração (sendo que esta equipa parou de existir). Nesse sentido, a equipa apenas apresenta publicações nesse último mês.
Tendo em conta o referido acima, os membros da equipa sentiram dificuldade em verificar a integridade dos requisitos e dos issues feitos pelos membros do grupo, uma vez que no momento em que a equipa de Q/A do Processo foi criada, esses mesmos requisitos e issues já se encontravam submetidos. Não obstante, a equipa conseguiu encontrar algumas falhas nos mesmos tendo sido corrigidas como por exemplo o esforço estimado em minutos e não em pommodoros.
Concluindo, esta equipa apresentou resultados satisfatórios tendo conseguido entregar os relatórios e o diagrama pedidos dentro da  reduzida deadline definida.

**[DESIGN]**

De uma forma geral, esta equipa trabalhou conforme o esperado e de forma satisfatória, tendo em conta que no início tinha poucas tarefas atribuídas para realizar.
No entanto, e assim que o projeto foi avançando e ganhando forma, foram surgindo questões e problemas que a equipa teve de resolver sendo que o trabalho foi distribuído por todos os membros de forma justa e equilibrada.
Foram desenvolvidos vários materiais em conjunto para que a imagem do projeto fosse coesa e apelativa e, assim que foram definidas regras e normas do uso da imagem, o trabalho seguinte foi feito de forma mais independente. 
Concluindo, o resultado final foi satisfatório tal como o desempenho da equipa, uma vez que todos os membros se empenharam para apresentar trabalho e contribuir para um projeto funcional.

**[PM]**
Como em todas as equipas referidas anteriormente, a equipa de Gestão do Projeto apresentou evidentes melhorias ao longo do semestre, sendo que no início se sentiu dificuldade em chegar a um consenso em como dividir os elementos em várias equipas e em como avançar no projeto.
Com o avançar das semanas a equipa de PMs conseguiu ter uma melhor perspetiva de como gerir a equipa e orientar todos os elementos de modo a avançar no projeto, tendo sido redefinido todo o processo de trabalho do grupo e feita uma reorganização das equipas. 
Esta reorganização definiu melhor o papel de cada elemento no grupo mas por outro lado, dado as alterações imprevistas, os membros que mudaram de equipas no final do processo terão pouco trabalho para apresentar.
O controlo de todas as tabelas foi sempre controlado pela equipa de PM tanto como a definição da equipa para a Validação Independente. 
