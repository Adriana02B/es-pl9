# GitNub
---
## Procedimento de criação de requisitos
1. [Primeiros passos](#Primeiros-passos)
2. [Formação do Requisito](#Inicialização-do-Requisito)
    1. [Criação de User Stories](#Criação-de-User-Stories)
    2. [Criação de Testes](#Criação-de-Testes)


### Primeiros passos
Para a criação de um novo requisito, deve ser lida/estudada a informação que nos é feita chegar pelo cliente.
 

### Inicialização do Requisito
Como inicialização de um requisito deve ser criado um **issue**;
- Título - deve ser a funcionalidade a apresentar;
- Label - corretamente referenciada como "Requirements";
- Assignee - o próprio criador;

- Descrição desse Issue devem estar presentes dois tópicos principais, sob esta forma:

    - User Storie: {[Criação de User Stories](#Criação-de-User-Stories)}

    - Testes:
        - Descrição teste 1:
            - Ação 1
            - Ação 2
            - ...
        - Descrição teste 2:
            - Ação 1
            - Ação 2
            - ...

**Nota**: Para cada Requisito/User Storie deve existir pelo menos um **teste**!

#### Criação de User Stories
Uma User Storie, deve ser uma frase simples e curta (o suficiente para caber num post it) , sob a forma:

##### "Como *{papel}* deve ser possivel/quero *{funcionalidade}* porque/para *{benefício}*."

#### Criação de Testes

Como mecanismo de simplificação, cada teste deve ser elaborado tendo como base a técnica de Desenvolvimento Guiado por Comportamento ([Behavior Driven Development (BDD) and Functional Testing](https://medium.com/javascript-scene/behavior-driven-development-bdd-and-functional-testing-62084ad7f1f2))
