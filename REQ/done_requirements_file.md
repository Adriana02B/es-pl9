# GIT NUB - Documento de requisitos feitos
---

**Versão de documento:** 1.0

**Data da última modificação:** 28-11-2021

**Estado**: Em aberto 

**Autor/es do documento**:<ul><li>Pedro Henriques [@pedroduartesh](https://gitlab.com/pedroduartesh)<br><li>Pedro Chaves [@PedrorChaves](https://gitlab.com/PedrorChaves)


## Tabela de conteúdos

[TOC]



## Esquema de Papeis 
```mermaid
graph TD
    A{Roles} --> C(Utilizador)
    C(Utilizador) --> F(Tem acesso ao repositório de produto)
```

## Design Mockups
---
### Landing Page
![Landing_Mockup](../DESIGN/Mockups/Issue_56_Landing_Page.png)
---
### Membros ativos
![Membros_ativos](../DESIGN/Mockups/Repository___Active_Members.png)
---
### Commits de membros ativos
![COMMITS_Membros_ativos](../DESIGN/Mockups/Repository___Active_Members_Commits.png)
---
### Listagem de Issues
![Listagem_Issues](../DESIGN/Mockups/Issue_67_All.png)


## Tabela de conteúdos
| **Issue** | **Categoria** | **Título** | **User Storie** |
| --- | --- | :---: |--- |
| #56 |**Landing Page**| Landing page | Como **utilizador**, gostava de ver uma landing page para conhecer o estado do projeto|
| #44 | **Ficheiros**     |    Deve ser apresentada a árvore de ficheiros do repositório |    Como **utilizador**, quero ver a árvore de ficheiros do mesmo, isto é, verificar um diagrama em árvore com as diretorias e respetivos ficheiros para ter uma visualização de todo o repositório e porque se torna mais simples encontrar ficheiros e as suas diretorias. Também quero poder diferenciar as diretorias dos ficheiros para identificação mais fácil .|
| #106 |**Membros**|Deverá existir uma página com a lista de membros activos do repositório | Como **Utilizador** quero que haja uma página com a lista de membros ativos no repositório, de modo que seja possível ver os membros que tenham feito pelo menos um commit.|
| #51 | **Membros** |  Deverá ser possivel observar a lista de commits de cada membro activo.|     Como **utilizador**, pretendo observar a lista de commits de cada um dos membros ativos, podendo organizar cada utilizador pelo seu histórico de commits.|
| #46 | **Issues**|  Possibilidade de visualizar a timeline de issues ao longo do tempo: abertos, activos e total. |     Como **utilizador**, deve ser possível visualizar a timeline de todos os issues pois é mais prático e torna a compreensão mais fácil.|
| #36 |**Exibição de dados solicitados**|Funcionalidade "back"    |Como **utilizador**, quero poder regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização, de modo a poupar tempo e aumentar produtividade.|
