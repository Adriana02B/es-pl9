# Design
  
## Equipa de Design: 
- <ins>Margarida Souto</ins>
- Inês Galvão
- Maria Raposo

## Processo:
- Desenvolvimento de código CSS e HTML;
- Criação de mockups.

## Organização da pasta:
Nesta pasta encontram-se os ficheiros referentes à equipa de design visual.
