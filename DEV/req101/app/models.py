from django.contrib import admin
from django.db import models
from datetime import datetime
# Create your models here.

class LocalRepo(models.Model):
    url = models.CharField(max_length=255, unique=True)
    path = models.CharField(max_length=255, unique=True)
    timestamp = models.DateTimeField(auto_now=True, verbose_name='Last Updated')

class ProjectMember(models.Model):
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255, unique=True)
    email = models.CharField(max_length=255, unique=True)