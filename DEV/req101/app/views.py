from django.http.request import HttpRequest
from django.shortcuts import redirect, render, HttpResponse
from pydriller import Repository
from git.repo import Repo
import logging

from app.models import ProjectMember
from . import gitlab
from . import utils 
from .forms import Picker

logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: Waiting for ARCH's final say
def listMembers(request: HttpRequest) -> HttpResponse:
    members = gitlab.getMembers()
    if members is None: return render(request, 'app/request.html', {'error': 'Algo correu mal'})
    members.sort(key=lambda x: x.name.lower())
    # Waiting for design and arch input
    # return HttpResponse("Waiting for design and arch input")
    return render(request, 'app/members.html', {'members': members})
    
def listMemberCommits(request: HttpRequest, username: str) -> HttpResponse:
    orderType = request.GET.get('order', None)
    branch = request.GET.get('branch') or 'main'
    order = "date_order" if orderType == 'oldest' else 'reverse'

    git = Repo(utils.getRepoPath(gitlab.REPO_URL))
    orderSet = [('newest', 'Newest to Oldest'), ('oldest', 'Oldest to Newest')]
    if orderType == 'oldest': orderSet.reverse()
    branchesSet = [branch,]
    for ref in git.heads:
        if str(ref) not in branchesSet: branchesSet.append(str(ref))
    form = Picker(None, branchesSet) 
    
    commits = []
    try:
        repo = Repository(utils.getRepoPath(gitlab.REPO_URL), only_in_branch=branch, order=order)
    except:
        return render(request, 'app/error.html', {'error': 'Branch does not exist!'})

    dbquery = ProjectMember.objects.filter(username=username)
    email: str
    name: str
    if dbquery.exists():
        email = dbquery[0].email
        name = dbquery[0].name
    else: 
        return render(request, 'app/error.html', {'message': 'Failed to retrieve user\'s email!'})

    for commit in repo.traverse_commits():
        if commit.author.email == email: commits.append(commit.msg)
    
    context = {
        'name': name,
        'commits': commits,
        'form': form
    }
    return render(request, 'app/list.html', context)