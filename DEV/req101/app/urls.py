from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    # !Waiting for design and arch input!
    path('members', views.listMembers, name='members'),
    path('member/<str:username>/commits', views.listMemberCommits, name='memberCommits'),
]