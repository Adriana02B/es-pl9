from typing import List
import requests
from .models import ProjectMember

REPO_URL = "https://gitlab.com/Adriana02B/es-pl9"
PRIVATE_TOKEN = "glpat-SNyaPe5sK6Lx3316WiWr"
REPO_ID = "30034710"
API_URL = f"https://gitlab.com/api/v4/projects/{REPO_ID}"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

class Member:
    name: str
    username: str
    email: str

    def __init__(self, name: str, username: str, email: str):
        self.name = name
        self.username = username
        self.email = email
    
    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

def getMembers() -> List[Member]:
    URL = f"{API_URL}/members/?per_page=100&page="
    data = []
    members = []
    i = 1
    while True:
        try:
            req = requests.get(
                f"{URL}{i}",
                headers=HEADERS
            )
            json = req.json()
            if json != []: data += json
            else: break
            i += 1
        except:
            return None
    for object in data:
        name = object.get('name')
        username = object.get('username')
        print(f"Name: {name}\nUsername: {username}\n")
        query = ProjectMember.objects.filter(name=name)
        if query.exists():
            pmember: ProjectMember = query[0]
            email = pmember.email
            print(f"Email:{email}\n\n")
        else: email = None
        members.append(Member(name, username, email))
    return members
