from django.urls import path, re_path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('example', views.template_example, name='example'),
    path('about', views.about, name='about'),
    path('repository', views.repository, name='repositoryMain'),
    re_path(r'^repository(?P<full_slug>(.*))/$', views.repository, name='repository'),
]