import logging
from django.shortcuts import render, HttpResponse
from django.http.response import Http404, HttpResponseNotFound
from git.exc import GitCommandError, GitError
from pydriller import Repository
from pydriller import Git

import app
from .models import LocalRepo
from django.http import Http404
from django.shortcuts import get_object_or_404
from .utils import getRepository

logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: /example
def template_example(request):
    text = "This is a Django template example"
    data = ["Try", "Out", "This", "For", "Loop"]
    return render(request, 'app/example.html', {'text': text, 'data': data})

# route: /about
def about(request):
    return render(request, 'app/about.html')

# route: /repository
def repository(request, full_slug):
    #serve para no futuro existir possibilidade de abrirmos mais repositórios
    text = "Repository"
    owner = "Adriana02B"
    repo = "es-pl9"
    branch = "main"

    data, htmlFile, text = getRepository(full_slug, owner, repo, branch)

    return render(request, htmlFile, {'text': text, 'data': data})