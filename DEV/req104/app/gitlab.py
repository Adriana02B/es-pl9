import logging
import re
import requests
import markdown
from app import utils

logger = logging.getLogger(__name__)
REPO_URL = "https://gitlab.com/Adriana02B/es-pl9"
PRIVATE_TOKEN = "glpat-SNyaPe5sK6Lx3316WiWr"
REPO_ID = "30034710"
API_URL = f"https://gitlab.com/api/v4/projects/{REPO_ID}"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

def getRepository(path, branch):
    htmlFile = "app/repository.html"
    URL = f"{API_URL}/repository/tree"

    #adiciona a pasta e/ou branch a usar
    if ((path != None or path != "/") and branch != None):
        URL = f"{URL}?path={path}&ref={branch}"
    elif (path != None or path != "/"):
        URL = f"{URL}?path={path}"
    elif (branch != None):
        URL = f"{URL}?ref={branch}"

    data = []

    req = requests.get(f"{URL}", headers=HEADERS)
    json = req.json()

    for object in json:
        folder = utils.File("/" + object["path"], object["name"])
        data.append(folder)

    return data, htmlFile, path

def getFile(path, branch):
    htmlFile = "app/files.html"

    folder = path.replace('/','%2F')
    folder = folder.replace(' ','%20')

    URL = f"{API_URL}/repository/files/"+ folder +"/raw?ref=" + branch

    lastItem = path.split("/")[-1]
    
    try:
        req = requests.get(f"{URL}", headers=HEADERS)
        text = req.text
        
        #verifica se e markdown ou html ou imagem ou um ficheiro comum
        if(lastItem.split(".")[1] == "md"):
            htmlReturn = markdown.markdown(text, extensions=['tables', 'extra', 'abbr', 'attr_list', 'def_list', 'fenced_code', 'footnotes', 'md_in_html', 'admonition', 'codehilite', 'legacy_attrs', 'legacy_em', 'meta', 'nl2br', 'sane_lists', 'smarty', 'toc', 'wikilinks'])
        
        elif(lastItem.split(".")[1] == "html"):
            htmlFile = "app/filesNoHtml.html"
            htmlReturn = text
        
        elif(lastItem.split(".")[1] in ["png", "svg", "jpg"]):
            htmlReturn = "<H4 style= 'color:Red'>"+ path +" Impossivel Abrir Formato de Ficheiro</H4>" #mensagem de erro
        
        else:
            #prepara o ficheiro para html
            text = text.replace('\n','<br>')
            text = text.replace('\t','&#9')
            text = "<pre>" + text + "</pre>"
            htmlReturn = text
        
    except:
        htmlReturn = "<H4 style= 'color:Red'>"+ path +" Impossivel Abrir Formato de Ficheiro</H4>" #mensagem de erro

    return htmlReturn, htmlFile, path


def getFileExiste(path, branch):
    lastItem = path.split("/")[-1]
    pathLast = path.replace(lastItem,'')

    data = getRepository(pathLast, branch)

    for i in data[0]:
        if(lastItem == i.name):
            return True

    return False