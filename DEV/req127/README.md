# Django Boilerplate

Isto é um projeto base, pronto para desenvolvimento com o vosso editor preferido :)

## Instruções 

### Setup inicial

Devem de ter o `pipenv` instalado. 

Para o fazer:
```bash
# Caso tenham apenas uma versão de python instalado
pip install pipenv

# Caso contrário
py -m pip install pipenv
```

Para instalar as dependências, num terminal dentro do diretório:
```bash
pipenv install
```

De seguida, devem correr as migrações para a base de dados:
```bash
pipenv run migrations
pipenv run migrate
```

### Development Server

Para verem as vistas no browser (localhost:8000):
```bash
pipenv run server
```

## Nota

No VS Code, o interpretador de Python por default não é o do venv. Devem de trocar pelo interpretador do venv criado pelo pipenv.
