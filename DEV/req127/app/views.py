import logging
from django.shortcuts import render
from git.repo import Repo
from git.util import T
from pydriller import Repository
from .utils import getRepoPath
from . import gitlab


logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: /example
def template_example(request):
    text = "This is a Django template example"
    data = ["Try", "Out", "This", "For", "Loop"]
    return render(request, 'app/example.html', {'text': text, 'data': data})

# route: /about
def about(request):
    return render(request, 'app/about.html')

def getFilePaths(repo: Repo, paths):
    added = False
    commits = list(map(str, repo.iter_commits(paths=paths)))
    drill = Repository(getRepoPath(gitlab.REPO_URL), only_commits=commits)
    for commit in drill.traverse_commits():
        for m in commit.modified_files:
            if m.new_path in paths or m.old_path in paths:
                if m.old_path not in paths: 
                    paths.append(m.old_path)
                    added = True
    if not added: return commits
    else: return getFilePaths(repo, paths)
    


def modList(request):
    """
    name = "MinuteMeeting12.md" # for testing purposes only, it should be passed as an argument in the future to be generalized for any file
    for commit in Repository(getRepoPath(gitlab.REPO_URL), only_no_merge=True, only_modifications_with_file_types=name.split('.')[-1]).traverse_commits(): # should be changed to local repo
                continue
    return render(request, "app/modifications.html", {'text': "Modifications for file " + name, 'data': files})
    """
    files =  []
    repo = Repo(getRepoPath(gitlab.REPO_URL))
    #path = "DEV/READme.md"
    path = "MINUTES/REVIEWED/MinuteMeeting13.md"
    paths = [path, path.replace('/', '\\')]
    name = path.split('/')[-1]

    # commits_touching_path = list(map(str, repo.iter_commits(paths=path)))
    # print(commits_touching_path)
    commits_touching_path = getFilePaths(repo, paths)
    #print(commits_touching_path)
    drill = Repository(getRepoPath(gitlab.REPO_URL), order='reverse', only_commits=commits_touching_path)
    for commit in drill.traverse_commits():
        for m in commit.modified_files:
            if m.new_path in paths or m.old_path in paths:
                if m.change_type.name == "MODIFY":
                    type = "modified"
                elif m.change_type.name == "ADD":
                    type = "added"
                elif m.change_type.name == "DELETE":
                    type = "deleted"
                elif m.change_type.name == "COPY":
                    type = "copied"
                elif m.change_type.name == "RENAME":
                    type = "renamed"
                
                files.append({
                    'file': m.filename,
                    'author': commit.author.name,
                    'date': commit.author_date,
                    'type': type,
                    'add': m.added_lines,
                    'del': m.deleted_lines
                })
    return render(request, "app/modifications.html", {'text': f"Modifications for file {name}", 'data': files})
