from typing import List
import requests
from datetime import datetime
import logging

logger = logging.getLogger(__name__)

REPO_URL = "https://gitlab.com/Adriana02B/es-pl9"
PRIVATE_TOKEN = "glpat-SNyaPe5sK6Lx3316WiWr"
REPO_ID = "30034710"
API_URL = f"https://gitlab.com/api/v4/projects/{REPO_ID}"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

class Member:
    name: str
    email: str

    def __init__(self, name, email):
        self.name = name
        self.email = email
    
    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

class Issue:
    title: str
    id: int
    cd: datetime
    authorn: str
    ud: datetime
    state: str

    def __init__(self, title, id, cd, authorn, ud, state):
        self.title = title
        self.id = id
        self.cd = cd
        self.authorn = authorn
        self.ud = ud
        self.state = state

def getIssues(state: str = None) ->List[Issue]:
    i = 1
    URL = f"{API_URL}/issues/?per_page=100"
    if state is not None:
        URL = URL + f"&state={state}"
    data = []
    issues = []
    while True:
        try:
            req = requests.get(
                f"{URL}&page={i}",
                headers=HEADERS
            )
            json = req.json()
            i += 1
            if json != []: data += json
            else: break
        except:
            logger.exception("Caught an exception")
            return None
    for object in data:
        title = object.get("title")
        id = object.get("id")
        cd = object.get("created_at")
        cd = cd[:-5]
        cd = datetime.strptime(cd, "%Y-%m-%dT%H:%M:%S")
        authorn = object.get("author").get('name')
        ud = object.get('updated_at')
        ud = ud[:-5]
        ud = datetime.strptime(ud, "%Y-%m-%dT%H:%M:%S")
        state = object.get("state")
        issues.append(Issue(title,id,cd,authorn,ud,state))
    return issues