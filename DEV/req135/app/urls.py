from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('example', views.template_example, name='example'),
    path('about', views.about, name='about'),
    path('issues', views.listAllIssues, name='issues'),
    path('issues/open', views.listOpenIssues, name='openIssues'),
    path('issues/closed', views.listClosedIssues, name='closedIssues'),
]