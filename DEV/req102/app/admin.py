from django.contrib import admin
from .models import LocalRepo

# Register your models here.
@admin.register(LocalRepo)
class LocalRepoAdmin(admin.ModelAdmin):
    list_display = ('path', 'url', 'timestamp')
