import logging
from django.shortcuts import render
from pydriller import Repository
from .utils import getRepoPath
from . import gitlab


logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: /example
def template_example(request):
    text = "This is a Django template example"
    data = ["Try", "Out", "This", "For", "Loop"]
    return render(request, 'app/example.html', {'text': text, 'data': data})

# route: /about
def about(request):
    return render(request, 'app/about.html')

"""
def commit_list(request):
    #repo = Repo.clone_from('https://gitlab.com/Adriana02B/es-pl9/', 'C:/Users/35192/Documents/UNI/3rd Year/1st Semester/ES/new')
    # repo = Repo('/tmp/xxx')
    #commits = list(repo.iter_commits('master', max_count=5))
    repo_path = 'C:/Users/35192/Documents/UNI/3rd Year/1st Semester/ES/new'
    repo = Repo(repo_path)
    max = 23

    commits_list = list(repo.iter_commits('main', max_count=max, skip = 150))
    commit = []
    commit_author = []
    #print(commit_list)
    for i in range(max):
        info = "Author: " + str(commits_list[i].author) + " >  " + str(commits_list[i].message)
        commit.append(info)
        #commit_author.append(commits_list[i].author)

        #author = repo.git.show("-s", "--format=Author: %an <%ae>", commit.hexsha)
        #print(author)
    #commits = list(repo.iter_commits(rev=repo.branches["devel"]))
    #data = [{'name': str(c.author), 'email': c.author.email, 'msg': c.message} for c in commits]
    #for c in commits:
        #print("Commit Message: " + str(c.message))
    return render(request, "app/commit.html", {'text': "Commit List", 'data': commit})

    """



# to change -> path: GitNub>Repository>Commits

def commit_list(request):
    commit = []
    for element in Repository(getRepoPath(gitlab.REPO_URL), order = 'reverse').traverse_commits():
        commit.append({
            'msg': element.msg,
            'author': element.committer.name,
            'date': element.committer_date
        })
    return render(request, "app/commit.html", {'text': "Commit List", 'data': commit})