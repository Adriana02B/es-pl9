import logging
from datetime import datetime, timedelta
import shutil
from git import Repo
from git.exc import GitCommandError, GitError, InvalidGitRepositoryError
from django.utils import timezone

from .models import LocalRepo

logger = logging.getLogger(__name__)

def clone(url, owner, repo):
    path = f"git/{owner}/{repo}"
    try:
        Repo.clone_from(url, path)
        entry = LocalRepo(url=url, path=path)
        entry.save()
    except GitCommandError:
        logger.error(f"Clone {url} failed!")
        try:
            shutil.rmtree(f"git/{owner}/{repo}") 
        except:
            logger.error("Failed to remove folder")
        return None
    return f"git/{owner}/{repo}"

def update(url: str):
    query = LocalRepo.objects.filter(url=url)
    if not query.exists(): return

    entry: LocalRepo = query[0]
    repo = Repo(entry.path)
    try:
        repo.remotes.origin.pull()
        logger.info(f"Updated repo {url}")
    except:
        # Pass the exception to be visible somewhere
        pass    
    entry.save()

def repo_get(url: str, owner: str, repo: str):
    query = LocalRepo.objects.filter(url=url)
    if query.exists():
        entry: LocalRepo = query[0]
        logger.info("Git repo already exists on local machine")
        path = entry.path
        if entry.timestamp + timedelta(hours=12) < timezone.now():
            update(entry.url)
    else:
        path = clone(url, owner, repo)
        if path is None:
            return None
        logger.info("Git repo cloned to the local machine")
    return path