import logging
from django.shortcuts import render, HttpResponse
from django.http.response import Http404, HttpResponseNotFound
from git.exc import GitCommandError, GitError
from pydriller import Repository, Git
from .models import LocalRepo
from .utils import clone, repo_get
from git import Repo, Commit
from pydriller.metrics.process.commits_count import CommitsCount


logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: /example
def template_example(request):
    text = "This is a Django template example"
    data = ["Try", "Out", "This", "For", "Loop"]
    return render(request, 'app/example.html', {'text': text, 'data': data})

# route: /about
def about(request):
    return render(request, 'app/about.html')

def files_list(request):  
    git = repo_get(url = "https://gitlab.com/" + "Adriana02B" + "/" +"es-pl9",owner = "Adriana02B", repo = "es-pl9")
    #git = './git/Adriana02B/es-pl9/'
    gr = Git(git)

    #Buscar os ficheiros e o numero de alterações - guarda dicionario files
    #Devolve alguns ficheiros nao existentes
    first_commit = None
    for commit in Repository(git, only_in_branch= "main").traverse_commits():
        first_commit = commit.hash
        break
    last_commit = None
    for commit in Repository(git, only_in_branch= "main", order= 'reverse').traverse_commits():
        last_commit = commit.hash
        break
    metric = CommitsCount(path_to_repo=git,
                        from_commit=first_commit,
                        to_commit=last_commit)
    files = metric.count()
    #Organiza o dicionario para ficarem os mais alterados primeiros
    final_dic = {}
    for i in sorted(files, key = files.get, reverse=True):
        final_dic[i] = files[i]

    #Buscar os ficheiros que existem e guarda na lista paths
    paths = []
    for path in gr.files():
        path_split = path.split('/')
        index = path_split.index("Adriana02B")
    
        paths.append(('/'.join(path_split[index+2:])))
    #Adiciona os ficheiros ja organizados à lista.Não adiciona os ficheiros que não existem
    files_organized = []
    for key in final_dic.keys():
        if key is not None:
            if key in paths:
                files_organized.append(key)
    return render(request, "app/commit.html", {'text': "Files List", 'files_organized': files_organized})