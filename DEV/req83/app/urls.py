from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('example', views.template_example, name='example'),
    path('about', views.about, name='about'),
    path('files', views.files_list, name='files'),
]