from django.db import models
# Create your models here.

class LocalRepo(models.Model):
    url = models.CharField(max_length=255, unique=True)
    path = models.CharField(max_length=255, unique=True)
    timestamp = models.DateTimeField(auto_now=True, verbose_name='Last Updated')
