import logging
import shutil
from git import Repo
from git.exc import GitCommandError
from git.objects.submodule.root import BRANCHCHANGE
from pydriller import Repository
from datetime import timedelta
from django.utils import timezone

from app import gitlab

from .models import LocalRepo

import requests

logger = logging.getLogger(__name__)

def clone(url: str) -> str:
    split = url.split('/')
    owner = split[-2]
    repo = split[-1]
    path = f"git/{owner}/{repo}"
    try:
        repo = Repo.clone_from(url, path)
        repo.remotes.origin.fetch()
        entry = LocalRepo(url=url, path=path)
        entry.save()
    except GitCommandError:
        logger.error(f"Clone {url} failed!")
        try:
            shutil.rmtree(f"git/{owner}/{repo}")
        except:
            logger.error("Failed to remove folder")
        return None
    return f"git/{owner}/{repo}"

def updateBranches(path: str):
    repo = Repo(path)
    localRefs = repo.heads
    for branch in repo.remotes.origin.refs:
        branchName = branch.name.split('/')[1]
        if branch.name == 'origin/HEAD' or branchName in localRefs: 
            logger.info(f'Branch {branch.name} already on local refs')
            continue
        repo.git.checkout('-b', branchName, branch.name)
        logger.info(f'Checked out remote branch {branch.name}')

def update(url: str):
    query = LocalRepo.objects.filter(url=url)
    if not query.exists(): return

    entry: LocalRepo = query[0]
    repo = Repo(entry.path)
    try:
        repo.remotes.origin.pull()
        logger.info(f"Updated repo {url}")
        updateBranches(entry.path)
        entry.save()
    except:
        logger.error(f'Failed to pull from remote {url}')

def getRepoPath(url: str):
    query = LocalRepo.objects.filter(url=url)
    if query.exists():
        entry: LocalRepo = query[0]
        logger.info("Git repo already exists on local machine")
        path = entry.path
        if entry.timestamp + timedelta(hours=12) < timezone.now():
            update(entry.url)
    else:
        path = clone(url)
        if path is None:
            return None
        logger.info("Git repo cloned to the local machine")
    return path
        

def checkoutBranch(path: str, branch: str = None) -> Repo:
    repo = Repo(path)
    if branch is not None and branch not in repo.branches:
        repo.git.checkout('-b', branch, f'origin/{branch}')
        logger.info(f'Remote branch {branch} checked out')
    else: logger.info('Branch already in local refs')




#------- Req 87 -------




class File:
    def __init__(self, url, name, author):
        self.url = url
        self.name = name
        self.author = author

    def __repr__(self):
        return self.url + " Author: " + self.author

def getBlame(object, branch):
    path = object["path"]
    path = path.replace("/", "%2F")

    URL = f"{gitlab.API_URL}/repository/files/" + path +"/blame?ref="+ branch

    req = requests.get(f"{URL}", headers=gitlab.HEADERS)
    json = req.json()

    firstAuthor = ""
    firstDate = ""

    for object in json:
        author = object["commit"]["committer_name"]
        date = object["commit"]["committed_date"]

        if(firstAuthor == "" and firstDate == ""):
            firstAuthor = author
            firstDate = date
        elif(firstDate > date):
            firstAuthor = author
            firstDate = date
    
    return firstAuthor
    
def getFilesByAuthor(branch):
    numeroPage = 2

    URLFIRST = f"{gitlab.API_URL}/repository/tree?recursive=true&page=1&per_page=1000"
    #adiciona a pasta e/ou branch a usar
    if (branch != None):
        URLFIRST = f"{URLFIRST}&ref={branch}"
    req = requests.get(f"{URLFIRST}", headers=gitlab.HEADERS)
    json = req.json()

    data = []
    a = 0

    while json != []:
        URL = f"{gitlab.API_URL}/repository/tree?recursive=true&page=" + str(numeroPage) + "&per_page=1000"
        numeroPage = numeroPage + 1

        #adiciona a pasta e/ou branch a usar
        if (branch != None):
            URL = f"{URL}&ref={branch}"
        req = requests.get(f"{URL}", headers=gitlab.HEADERS)
        json = req.json()

        for object in json:
            if(object["type"] == "blob"):
                author = getBlame(object, branch)
                file = File("/" + object["path"], object["name"], author)
                data.append(file)

    return data