from django.urls import path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('example', views.template_example, name='example'),
    path('about', views.about, name='about'),
    path('filesbyauthor', views.getFilesByAuthor, name='getFilesByAuthor'),
]