from django.http.request import HttpRequest
from django.shortcuts import redirect, render, HttpResponse
from pydriller import Repository
from git.repo import Repo
import logging
from . import gitlab
from . import utils

logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: /example
def template_example(request):
    text = "This is a Django template example"
    data = ["Try", "Out", "This", "For", "Loop"]
    return render(request, 'app/example.html', {'text': text, 'data': data})

# route: /about
def about(request):
    return render(request, 'app/about.html')


def listBranches(request: HttpRequest) -> HttpResponse:
    branches = gitlab.getBranches()
    if branches is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    return render(request, 'app/branch.html', {'branches': branches})
