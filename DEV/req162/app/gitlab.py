from typing import List
import requests
from datetime import datetime
import logging

logger = logging.getLogger(__name__)
REPO_URL = "https://gitlab.com/Adriana02B/es-pl9"
PRIVATE_TOKEN = "glpat-SNyaPe5sK6Lx3316WiWr"
REPO_ID = "30034710"
API_URL = f"https://gitlab.com/api/v4/projects/{REPO_ID}"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

class Branch:
    title: str
    lupdater: str
    ldate: datetime

    def __init__(self, title, lupdater, ldate):
        self.title = title
        self.lupdater = lupdater
        self.ldate = ldate

def getBranches() -> List[Branch]:
    i = 1
    URL = f"{API_URL}/repository/branches/?per_page=100"
    data = []
    branch = []
    while True:
        try:
            req = requests.get(
                f"{URL}&page={i}",
                headers=HEADERS
            )
            json = req.json()  #hello father how are u
            i += 1
            if json != []: data += json
            else: break
        except:
            logger.exception("Caught an exception")
            return None
    for object in data:
        name = object.get('name')
        author = object.get('commit').get('author_name')
        date: str = object.get('commit').get('committed_date')
        date = date[:-10]+date[-6:-3]+date[-2:]
        date = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S%z")
        branch.append(Branch(name, author, date))
    return branch
    