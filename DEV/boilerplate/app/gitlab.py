from typing import List
import requests

REPO_URL = "https://gitlab.com/Adriana02B/es-pl9"
PRIVATE_TOKEN = "glpat-SNyaPe5sK6Lx3316WiWr"
REPO_ID = "30034710"
API_URL = f"https://gitlab.com/api/v4/projects/{REPO_ID}"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

class Member:
    name: str
    email: str

    def __init__(self, name, email):
        self.name = name
        self.email = email
    
    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

def getMembers() -> List[Member]:
    URL = f"{API_URL}/members/?per_page=100&page="
    data = []
    members = []
    i = 1
    while True:
        try:
            req = requests.get(
                f"{URL}{i}",
                headers=HEADERS
            )
            json = req.json()
            if json != []: data += json
            else: break
            i += 1
        except:
            return None
    for object in data:
        members.append(Member(object.get('name'), object.get('email', None)))
    return members
