# Desenvolvimento

## Equipa de Desenvolvimento: 
- <ins>Rodrigo Machado</ins>
- Carlos Jordão
- David Leitão
- Gonçalo Folhas
- Rui Bernardo
- João Fernandes
- Samuel Pires

## Processo:
- Desenvolvimento de código backend (Django);
- Sempre que um código não passa nos testes, volta para a equipa de desenvolvimento.

## Organização da pasta:
A diretoria está organizada pelos vários requisitos, sendo que terá de ser criada uma nova diretoria para cada um. 
O nome da nova diretoria deve ser do tipo `req[nº issue]`. 

Por exemplo, `DEV/req50` será uma pasta dentro deste diretório, onde se encontra o código de desenvolvimento para o requisito pedido no *issue* número 50 com o label de `Developing`.
