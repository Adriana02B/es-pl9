## Tutorial:

Para escrever a primeira parte de testes de como iremos fazer os testes mais à frente em código, vamos fazer uma tabela para dividir os casos em _Inicialização_, _Input_ e _Output_.

Para terem noção de quais botoes e ícones devem apresentar nos testes vejam o que a malta de Design tem sobre esse Issue associado.

Criem um Issue ligado aos Issues da categoria de 'Developing', usem a label 'Testing', e têm de se por como Assignees.

Copiem o template e prencham de acordo com o vosso Issue escolhido.

No fim não se esqueçam de pôr o tempo que estiveram a trabalhar no issue. Basta escreverem "/spend" e o tempo à frente (Ex: /spend 2d 3h 10m, corresponde a 2 dias 3 horas e 10 minutos).

--------
### Issue: #90

Assumindo que há acesso há dashboard, apresentam-se os seguintes testes:

--------
| Nº teste | Inicialização | Input | Output |
| --- | --- | --- | --- |
| Teste 1 | Utilizador está logged-in. Ícone de Menu mostrado no ecra (barra superior, lado esquerdo). Ícone de Linguagem na lista do Menu mostrado no ecrã (barra lateral, centro). Lista de paises mostrado no ecra (barra lateral, lado direito). Português (PT) selecionada. | Clicar em Menu. Clicar em Linguagem. Clicar em Português (PT). | Opção PT fica selecionada. Menus passam para PT. |
| Teste 2 | Utilizador está logged-in. Ícone de Menu mostrado no ecra (barra superior, lado esquerdo). Ícone de Linguagem na lista do Menu mostrado no ecrã (barra lateral, centro). Lista de paises mostrado no ecra (barra lateral, lado direito). English (EN) selecionada. | Clicar em Menu. Clicar em Linguagem. Clicar em English (EN). | Opção EN fica selecionada. Menus passam para EN. | 
