# ES PL9 - GitNub

Repositório da turma PL9 da cadeira de Engenharia de Software. 

Aqui reside todo o trabalho feito pela equipa para este projeto:

[[MINUTES]](MINUTES):
- Atas;

[[PM]](PM):
- Gestão de recursos da equipa;
- Criação de documentos de organização.

[[REQ]](REQ):
- Criação e verificação de User Stories;
- Organização de requisitos.

[[ARCH]](ARCH):
- Definição do tipo de arquitetura a usar;
- Criação a partir dos User Stories verificação de requisitos de produto;
- Organização de requisitos de produto em endpoints.

[[DESIGN]](DESIGN)
- Desenvolvimento de código CSS e HTML;
- Criação de mockups.

[[DEV]](DEV)
- Desenvolvimento de código backend;

[[QA]](QA)
- Verificação do que é produzido pelas outras equipas [Qualidade de Processo];
- Criação e testagem de cada um dos requistos [Qualidade de Produto - Testes];


Integração e deploy do que foi desenvolvido até ao momento.
