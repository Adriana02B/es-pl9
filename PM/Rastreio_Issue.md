| Issues por Requisito                | Fase de processo              | Próxima fase de processo  |
|:-----------------------------------:|:-----------------------------:|:-------------------------:|
| #32  -> #68  -> #97  -> #90  -> #129| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #33  -> #69  -> #82  ->      -> #144| DESIGN, TESTING               | DEVELOPING
| #34  -> #70  ->      ->      -> #151| TESTING                       | DESIGN, DEVELOPING
| #35  -> #71  -> #93  -> #102 -> #133| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #36  -> #75  -> #95  ->      -> #149| DESIGN, TESTING               | DEVELOPING
| #37  -> #81  -> #91  -> #162 -> #150| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #38  -> #63  -> #83  -> #112 -> #134| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #39  -> #66  -> #89  -> #101 -> #131| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #42  -> #65  -> #84  -> #87  -> #136| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #43  -> #74  -> #123 ->      -> #143| DESIGN, TESTING               | DEVELOPING
| #44  -> #80  -> #98  -> #104 -> #132| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #45  -> #110 -> #128 ->      -> #147| DESIGN, TESTING               | DEVELOPING
| #46  -> #67  -> #94  -> #135 -> #142| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #49  -> #64  -> #92  -> #127 -> #137| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #50  -> #73  -> #88  -> #138 -> #141| DESIGN, DEVELOPING, TESTING   | INTEGRATION
| #51  -> #72  -> #124 ->      -> #145| DESIGN, TESTING               | DEVELOPING
| #54  -> #79  ->      ->      -> #156| TESTING                       | DESIGN, DEVELOPING 
| #103 ->      ->      ->      -> #164| TESTING                       | ARCHITECTURE, DESIGN, DEVELOPING
| #105 -> #115 -> #157 ->      -> #148| DESIGN, TESTING               | DEVELOPING
| #106 -> #120 -> #158 ->      -> #152| DESIGN, TESTING               | DEVELOPING 
| #107 -> #122 -> #159 ->      -> #155| DESIGN, TESTING               | DEVELOPING 
| #109 -> #121 -> #160 ->      -> #153| DESIGN, TESTING               | DEVELOPING 
| #114 -> #116 -> #161 ->      -> #154| DESIGN, TESTING               | DEVELOPING 

| Landing Page  | Descrição             |
|:-------------:|:---------------------:|
| #56 -> #99    | Landing Page (Mockup) |
| #56 -> #100   | Menu                  |
