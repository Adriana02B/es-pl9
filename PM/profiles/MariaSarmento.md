# Maria Sarmento

@sarmento13

Testers/Quality

mariasarmento1999@hotmail.com

Licenciatura em Engenharia Informática - 3º ano

---

## Contribuições 

### [ARCH]
- Issue #72
- Issue #71
- Issue #70
- Issue #69
- Issue #68
- Issue #67

### [QA - Produto]
- Issue #152
- Issue #133
- Issue #153

### [MINUTES]
- Commit: #d9e6b972 
- Redigi também a ata nº6;
- Revistas: Ata nº 4, nº 9;


### [Validação Independente]
- Issue #180


--- 
## Papel na Equipa
- Pertenci à equipa de Testes e apoiei a equipa de Arquitetura.
