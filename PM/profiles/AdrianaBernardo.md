# Adriana Bernardo
---

@Adriana02B

Project Manager

gbernardo@student.dei.uc.pt

Licenciatura em Engenharia Informática - 3º ano

---

## Contribuições 

### [PM]
- Issue #118 - Criação da Tabela de Atividade de Membros [Tabela](/PM/Atividade_membros.md)
- Issue #117 - Atualização das Tabelas (contém todos os commits)
- Issue #119 - Integração com o Discord 
- Issue #163 - READme Files de todas as pastas da equipa
- Issue #166 - Criação Template para Perfil [Template](/PM/profiles/template_final.md)
- Issue #167 - Documento Validação Independente - GitNub [Documento](/PM/Validação independente.docx)
- Issue #168 - Criação documento Validação independente - Dash'it 

### [REQ]
- Issue #37 - Requisito "*Escolher o branch que está a ser visualizado*"

### [ARCH]
- Issue #120 - Requisito de Produto

### [QA - Processo]
- Issue #140 - Criação do documento de processos da equipa [Documento](/PM/Processo.md)

--- 
## Papel na Equipa

- Fui Project Manager da equipa
