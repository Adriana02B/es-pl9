# Nome

Gitlab Nickname

Cargo(s)
 
Email

Curso

(Opcional) Biografia

---

## Contribuições 

**ATENÇÃO:**
**_ATÉ 7 entradas por categoria e cada entrada sempre associada a um link !_** 

As entradas devem ser de um dos seguintes tipos:
- Issue #X (colocar o número do Issue) 
- Ficheiro X (colocar o nome do Ficheiro)- Link para o Ficheiro no GitLab 
- Commit X (colocar o código do Commit) 

Como obter o link do ficheiro: 
1. Ir à pasta onde o ficheiro se encontra, abrir o ficheiro e clicar no botão "Copy file path" (junto ao nome do ficheiro);
2. Exemplo: Ficheiro [DESIGN/READme.md](url) entre parenteses escrever o URL;
3. Para perceberem melhor abrir em modo "Display source" também disponível ao lado da opção Edit do ficheiro.

### [PM]
- 

### [REQ]
-

### [ARCH]
-

### [DES]
-

### [DEV]
-

### [QA - Produto]
-

### [QA - Processo]
-

--- 
## Papel na Equipa

(Indicar a que equipa a que pertenceram)

Exemplos: 
- Pertenci à equipa de Desenvolvimento;
- Fui TL da equipa de Testes;
- Pertenci à equipa de Requisitos nas três primeiras semanas e à equipa de testes nas restantes semanas;
- Pertenci à equipa de Design e nas últimas duas semanas pertenci à equipa da Validação Independente;
