# Gonçalo Folhas

@Folhas

Developer
 
gtaff2001@gmail.com

Engenharia Informática

---

My name is Gonçalo Folhas, I'm a 20 year old student from Fermentelos and I'm looking forward to learning more during the completion of this project :) I've been programming for a few years but I have no experience with Django, hence why I am
currently learning more about it :)


---

## Contribuições 

### [PM]

- Ata nº 7 (Revi) - [Ata 7](/MINUTES/REVIEWED/MinuteMeeting07.md)
- Ata nº 9 (Revi) - [Ata 9](/MINUTES/REVIEWED/MinuteMeeting09.md)
- Ata nº 12 (Redigi) - [Ata 12](/MINUTES/REVIEWED/MinuteMeeting12.md)
- Ata nº 15 (Revi) - [Ata 15](/MINUTES/REVIEWED/MinuteMeeting15.md)

### [REQ]

- Requisito "*Issues associados a ficheiros*" - Issue #50

### [DEV]

- Issue #102 - [Requisito](/DEV/req102/)
- Issue #127 - [Requisito](/DEV/req127/)

--- 
## Papel na Equipa

- Pertenci à equipa de Desenvolvimento;
