# João Fernandes

@JoaoJoaoFernandes

Backend Developer
 
joaoladt@gmail.com

Licenciatura em Engenharia Informática - 3º ano

---

Hello my name is Joao Fernandes, and I have zero experience working with the django framework

## Contribuições 

### [REQ]
- **Issue #33**:<br>Criação de requisito

### [DEV]
- **Commit 437f9ca1879c8083af9ffd976d5ff43cda9bc864**: <br>Desenvolvimento do requisito **Issue #83**
- Participação no desenvolvimento do requisito **Issue #93**

### [PM]
- Co-autoria do [Template de atas](MINUTES/template.md)
- **Commit c66adffd71e208a0c4aca71fabdc66405ea8a927**:<br>Ata Reunião numero 4

--- 
## Papel na Equipa

- Pertenci à equipa de Desenvolvimento
