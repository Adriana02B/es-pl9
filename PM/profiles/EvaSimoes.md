# Eva Simões

@eva_simoes

Process Quality Assurance 

uc2019216962@student.uc.pt

Licenciatura em Engenharia Informática

---
My name is Eva Simões and I'm 20 years old.

## Contribuições 

### [ARCH]
- Issue #64
- Issue #65
- Issue #66
- Issue #81

### [QA - Produto]
- Issue #131
- Issue #148
- Issue #149

### [QA - Processo]
- Ficheiro [QA_Process/RelatorioProcessoEquipa.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/QA_Process/RelatorioProcessoEquipa.md)
- Ficheiro [QA_Process/ApreciacaoGlobalProcesso.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/QA_Process/ApreciacaoGlobalProcesso.md)
- Ficheiro [QA_Process/DiagramaProcesso.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/QA_Process/DiagramaProcesso.md)

### [ATAS]
- Ata nº 11 (Redigi) [MINUTES/REVIEWED/MinuteMeeting11.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting11.md)
- Ata nº 4 (Revi) [MINUTES/REVIEWED/MinuteMeeting04.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting04.md)
- Ata nº 9 (Revi) [MINUTES/REVIEWED/MinuteMeeting09.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting09.md)
- Ata nº 10 (Revi) [MINUTES/REVIEWED/MinuteMeeting10.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting10.md)
- Ata nº 12 (Revi) [MINUTES/REVIEWED/MinuteMeeting12.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting12.md)
- Ata nº 14 (Revi) [MINUTES/REVIEWED/MinuteMeeting14.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting14.md)
- Ata nº 16 (Revi) [MINUTES/REVIEWED/MinuteMeeting16.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting16.md)
- Ata nº 16 (Revi)

--- 
## Papel na Equipa

- Apoio à equipa de Arquitetura na execução de alguns Issues.
- Pertenci à equipa de Testing (QA - Produto) nas três primeiras semanas;
- TL da equipa de QA - Processo;
