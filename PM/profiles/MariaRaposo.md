# Nome

@mariafraposo

Designer
 
raposo101201@gmail.com

Licenciatura em Design e Multimédia (3º ano)

My name is Maria Raposo and I'm 20 years old. I am from Coimbra and I love Design and Volunteering

---

## Contribuições

### [PM]
- 

### [REQ]
-

### [ARCH]
-

### [DES]
- Issue #58 - [Manual de Normas Gráficas] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/ManualDeNormasGra%CC%81ficasGitNub.pdf)
- Issue #56 - [Landing Page] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/Mockups/Issue_56_Landing_Page.png)
- Issue #56 - [Menu] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/Mockups/Issue_56_Menu_Issues_Service_Desk.png)
- Issue #67 - [Open Issues] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/Mockups/Issue_67_Open.png)
- Issue #68 - [Menu Language] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/Mockups/Issue_68_Menu_Language.png)
- Issue #80 - [Category Diagram] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/Mockups/Issue_80_Category_Diagram.png)
- Issue #114 - [Contributions Category] (https://gitlab.com/Adriana02B/es-pl9/-/blob/design/DESIGN/Mockups/Issue_107_Contributions_Category.png)

### [DEV]
-

### [QA - Produto]
- Issue #196 - [Teste Requisito 12 Dash'It] (https://gitlab.com/Adriana02B/es-pl9/-/blob/Valida%C3%A7%C3%A3o_Independente/QA_Process/Val-Indep/REQ12/avaliacaoReq12.md) 

### [QA - Processo]

--- 
## Papel na Equipa

Durante todo este semestre, fiz parte da equipa de Design. A cargo da equipa de Design ficaram as mockups do projeto, pelo que estas foram distribuidas pelos 3 membros. Fiquei responsável por criar todo o design que me foi atribuido dos Requisitos de Produto, depois de trabalhados por Arquitetura, pelo que desenhei toda a componente visual e estética dos issues: #56, #68, #45, #80, #110, #67, #105, #114, #106, #109, #122 e #107.
Desenhei mockups dos mais variados tópicos, algumas de maior destaque, tais como o Menu Principal e a Landing Page.

Por fim, na última semana, decidi arriscar e juntei-me à equipa de Testes, pelo que trabalhei com o Requisito 12, da Equipa Dash'It. Explorei-o e avaliei-o.

Ao longo do semestre fui participando nas reuniões marcadas e acompanhando o trabalho de cada um, a dialogar e a ouvir os outros sempre que fosse preciso.

