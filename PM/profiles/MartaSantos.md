# Marta Santos

@marta-c-santos

Testers/Quality

martasantos@student.dei.uc.pt

Licenciatura em Engenharia Informática - 3º ano

---

## Contribuições 

### [PM]
- Ata nº 7  (revisão): [MinuteMeeting07.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting07.md)
- Ata nº 9  (revisão): [MinuteMeeting09.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting09.md)
- Ata nº 11 (revisão): [MinuteMeeting11.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting11.md)
- Ata nº 14 (revisão): [MinuteMeeting14.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting14.md)
- Ata nº 17 (revisão): [MinuteMeeting17.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting17.md)


### [ARCH]
- Issue #74
- Issue #77


### [QA - Produto]
- Ficheiro [Tabela_Casos_Testing.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/devel/QA/Tabela_Casos_Testing.md)
- Ficheiro [TestingTemplate.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/devel/QA/TestingTemplate.md)
- Issue #129
- Issue #137
- Issue #154
- Issue #177
- Validação do Requisito 31 da equipa Dash'It:
    - Ficheiro [avaliacao.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/Valida%C3%A7%C3%A3o_Independente/QA/Val-Indep/REQ%2030/avaliacao.md)
    - Ficheiro [avaliacaoReq30.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/Valida%C3%A7%C3%A3o_Independente/QA/Val-Indep/REQ%2030/avaliacaoReq30.md)
    - Ficheiro [tabelaReq30.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/Valida%C3%A7%C3%A3o_Independente/QA/Val-Indep/REQ%2030/tabelaReq30.md)


--- 
## Papel na Equipa
- Fui a TL da equipa de Qualidade do Produto (Testes).
- Apoio da equipa de Arquitetura na execução de alguns Issues.
- Integrei a equipa de Validação Independente nas duas últimas semanas
