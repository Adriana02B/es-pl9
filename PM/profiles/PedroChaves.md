## Pedro Chaves

@PedrorChaves

Requirements Team
 
pchaves.2001@gmail.com

Licenciatura em Engenharia Informática - 3º ano

My name is Pedro Chaves and I'm 20 years old.

---

## Contribuições 

### [REQ]
- Issue #34
- Issue #103
- Issue #105
- Issue #106
- Issue #107
- Issue #108
- Issue #109
- Issue #114
- Ficheiro [REQ/requirements_file.md](requirements_file.md)
- Ficheiro [REQ/done_requirements_file.md](done_requirements_file.md)


### [PM]
- Ata [MINUTES/REVIEWED/MinuteMeeting05.md](MinuteMeeting05.md)


### [TESTING]
- Requisito 16/3 - "Visualização de ficheiros por categoria"
    - Issue #184
    - Avaliação do requisito [QA_Process/Val-Indep/REQ-16-3/avaliacaoReq16-3.md](avaliacaoReq16-3.md)
    - Testes do requisito [QA_Process/Val-Indep/REQ-16-3/testesReq16-3.md](testesReq16-3.md)
    - Avaliação final do requisito [QA_Process/Val-Indep/REQ-16-3/avaliacaoFinal.md](avaliacaoFinal.md)

## Papel na Equipa

- Pertenci à equipa de Requerimentos;
- Nas últimas duas semanas pertenci à equipa de validação independente.
