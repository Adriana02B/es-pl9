# João Vaz


@Vaz07 

Developer

jvaz01012@gmail.com

Licenciatura em Engenharia Informática - 3.o ano

---

My name is João António Vaz, I'm 20 years old. 

I have some knowledge in HTML and CSS

Also, I love sports ^^

---

## Contribuições 

### [PM]
- Criação da Tabela de Rastreio - [Tabela](/PM/Rastreio_Issue.md)
- Criação da Tabela de Atividade de Membros (contribuição conjunta) [Tabela](/PM/Atividade_membros.md)
- Atualização das Tabelas - Issue #117 (contém todos os commits)
- Minute 4, Review - [Ata 4](/MINUTES/REVIEWED/MinuteMeeting04.md)
- Minute 8, Write - [Ata 8](/MINUTES/REVIEWED/MinuteMeeting08.md)
- Minute 10, Review - [Ata 10](/MINUTES/REVIEWED/MinuteMeeting10.md)
- Minute 13, Review - [Ata 13](/MINUTES/REVIEWED/MinuteMeeting13.md)
- Minute 16, Review - [Ata 16](/MINUTES/REVIEWED/MinuteMeeting16.md)

### [REQ]
- Requisito "*Deve ser apresentada a árvore de ficheiros do repositório*" - Issue #44

### [ARCH]
- Requisito de Produto - Issue #121
- Requisito de Produto - Issue #122

### [DES]

### [DEV]

### [QA - Produto]
- 1ª fase de Teste - Issue #155
- Validação Independente:
    - Requisito 29:
        - Issue #172
        - [Avaliação do requisito](/QA_Process/Val-Indep/avaliacao.md)
        - [Testes efetuados](/QA_Process/Val-Indep/requisito29-testes.md)
        - [Tabela de testes](/QA_Process/Val-Indep/tabela-req29.md)

### [QA - Processo]
-

--- 
## Papel na Equipa

- Nos primeiros dois meses pertenci à equipa de Desenvolvimento; 
- No último mês passei ao cargo de PM e ainda integrei a equipa de Validação Independente.
