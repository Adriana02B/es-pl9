# Rodrigo Machado

---

@bitl0ck

Project Manager

ramachado@student.dei.uc.pt

Licenciatura em Engenharia informática

---

My name is Rodrigo Alexandre da Mota Machado, I'm 21 years old. 

My only experience with the Django framework is their tutorial :)

## Contribuições

### [PM]

- Criação de issues ([exemplo](#1)) e [template](f1c5def3) para os perfis de membros

### [DEV]

- [Integração](#216) -> [Produto](PROD)
- Issue #101
- Pastas 
    - [boilerplate](DEV/boilerplate)
    - [101](DEV/101)

## Papel na equipa

- Assumi o papel de PM com a Adriana Bernardo no início do projeto (cerca de 2 meses)
- Assumi o papel de Dev Lead, trocando de papel com o João Vaz
