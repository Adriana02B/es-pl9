# Iago Silva Bebiano

@iagosilvabebiano

Tester/Quality

iagosilvabebiano@gmail.com

Licenciatura em Engenharia Informática

---

I'm 20 years old, huge sports fan, especially tenis and canoeing. I myself am a canoist , speed specialist (200 and 500m). 


---
## Contribuições 

### [PM]
- Minute 6, Review - [Ata 6](MINUTES/REVIEWED/MinuteMeeting06.md)
- Minute 7, Write - [Ata 7](MINUTES/REVIEWED/MinuteMeeting07.md)
- Minute 8, Write - [Ata 8](MINUTES/REVIEWED/MinuteMeeting08.md)
- Minute 12, Write - [Ata 12](MINUTES/REVIEWED/MinuteMeeting12.md)
- Minute 15, Write - [Ata 15](MINUTES/REVIEWED/MinuteMeeting15.md)
- Minute 16, Write - [Ata 16](/MINUTES/REVIEWED/MinuteMeeting16.md)



### [REQ]
- Requisito "*Possibilidade de visualizar a lista de contribuições por categoria (REQ, PM, DEV, TST...) de cada membro activo*" - Issue #39

### [QA - Produto]<br>
- Issue #112

- Issue #123

- Issue #124
<br>

- Issue #205
- Ficheiro [avaliacao.md](QA_Process/Val-Indep/REQ10/avaliação.md)
- Commit [66de6cc44cf23265bc5fe3b333f3f1c4385c5362]
<br>

- Issue #205
- Ficheiro [requisito10-testes.md](QA_Process/Val-Indep/REQ10/requisito10-testes.md)
- Commit [1522c16bb121908b34ab2f590e9dae7e42b4f74b]
<br>

- Issue #205
- Ficheiro [tabelaReq10.md](QA_Process/Val-Indep/REQ10/tabelaReq10.md)
- Commit [580aa517e7bccb34a531b372a82fa4f03fa509eb]
<br>

## Papel na Equipa

- Pertenci à equipa de Testers e integrei a equipa de Validação Independente.
