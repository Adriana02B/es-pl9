# Tiago Ventura


@Venturazz 

Quality/Testers

tiagofilipe122@gmail.com

Licenciatura em Engenharia Informática - 3.o ano

---

## Contribuições 

### [QA - Processo]
-Issue #156

-Issue #147

-Issue #146

### [Validação Independente]
- Requisito 20:
    - Issue #197
    - [Avaliação do requisito](/QA_Process/Val-Indep/avaliacao.md)
    - Commits : bcedaafbe950fd9e5567aea19cd92bfa135052be

    - [Testes efetuados](/QA_Process/Val-Indep/requisito20-testes.md)
    - Commits : 27d9f0eda563caf832c19ded279bdca00d91f1eb

    - [Tabela de testes](/QA_Process/Val-Indep/tabela-req20.md)
    - Commits : 1210643d704f034734754ddb09d3de85820ba37a

--- 
## Papel na Equipa

- Pertenci sempre à equipa de Testes e realizei as tarefas que me foram dadas e testes nos ficheiros que me foram atribuídos.
