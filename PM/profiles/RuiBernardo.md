# Rui Bernardo
---

@ruibernardo521

Developer
 
ruibernardo521@gmail.com

Licenciatura em Engenharia Informática - 3º ano

---

## Contribuições 

### [PM]
- Realização da [ata da 2ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting02.md).

- Realização da [ata da 9ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting09.md).

- Realização da [ata da 11ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting11.md).

- Realização da [ata da 14ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting14.md).

- Realização da [ata da 17ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting17.md).

### [DEV]
- Issue #138 

### [QA_Product]<br>
- Issue #198
- Ficheiro [avaliacao.md](QA_Process/Val-Indep/REQ 9/avaliacao.md)
- Commit [6355665bfec6f478a4bb6a1fc0e1bf29c5cc9e80]
<br>

- Issue #198
- Ficheiro [avaliacaoReq6.md](QA_Process/Val-Indep/REQ 9/avaliacaoReq6.md)
- Commit [340b5d50844484580590d5251f6a4209ee9c528a]
<br>

- Issue #198
- Ficheiro [tabela-req6.md](QA_Process/Val-Indep/REQ 9/tabela-req6.md)
- Commit [8e30c52c0e208f541f3d6dd7e7ea7c1033ba3a45]
<br>

--- 
## Papel na Equipa<br>

Membro da equipa de desenvolvimento.

Membro da equipa de Validação Independente.
