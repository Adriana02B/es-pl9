# Bernardo Arzileiro

@BennyAltF4

Arquitetura
 
bernardo.arzileiro@gmail.com

Engenharia Informática

Studying, Doing Sports or Creating Digital Content

---

## Contribuições 

### [PM]
- Realização da [ata](MINUTES/REVIEWED/MinuteMeeting02.md) da 2ª reunião da equipa.

### [REQ]
- Issue #49 - Visualização de um histórico de um ficheiro ao clicar no mesmo

### [ARCH]
- Issue #105 - Requisito Produto: #115
- Issue #114 - Requisito Produto: #116
- Organização de [Tabela de Requisitos de Produto](ARCH/Tabela Arch Requisitos.md) 
- Elaboração de [Diagrama de Arquitetura](ARCH/Diagrama_de_Arquitetura.pdf)

### [QA - Produto]
**Validação do Requisito 31 da equipa Dash'It:**
- [Testes](QA_Process/Val-Indep/REQ31/testesReq31.md)
- [Tabela de Testes](QA_Process/Val-Indep/REQ31/req31tabela.md)
- [Avaliação Final](QA_Process/Val-Indep/REQ31/avaliacaoReq31.md)

--- 
## Papel na Equipa

- Pertencia inicialmente à equipa de integração. Em Novembro a equipa sofreu um restruturação e a equipa de Integração deixou de existir. Nessa altura passsei a integrar a equipa de Arquitetura;
- Nas últimas duas semanas pertenci à equipa da Validação Independente.

