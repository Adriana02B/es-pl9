# Samuel Pires

@ItzPires

Developer
 
samuel4534@live.com.pt

Licenciatura em Engenharia Informática - 3º ano

---

## Contribuições 

### [REQ]
- Issue #32 - Suporte multi-idioma


### [DEV]
- Issue #87
- Issue #104


### [PM]
- Ata nº 5 (Redigi) [MINUTES/REVIEWED/MinuteMeeting05.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting05.md)
- Ata nº 8 (Revi) [MINUTES/REVIEWED/MinuteMeeting08.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/MINUTES/REVIEWED/MinuteMeeting08.md)
--- 
## Papel na Equipa

- Membro da equipa de Desenvolvimento;
