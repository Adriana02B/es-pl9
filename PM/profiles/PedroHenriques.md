# Pedro Henriques
---

@pedroduartesh 

Requirements Team

pedrohenriques@student.dei.uc.pt

Licenciatura em Engenharia Informática - 3º ano

---

My name is Pedro Henriques and I am 20 years old. Undergraduate student in Computer Engineering, i don't have any experience working with the front end or with the Django framework.

## Contribuições 

### [PM]
- **Commit d69128dac9a6734a6415bf8b07b91c46af4a77bc**: <br>Criação do esqueleto de repositório segundo template disponibilizada pelo professor
- **Commit 583f8d3291aae968df09a7812ce5d6d683d29d8a**:<br>Ata Reunião numero 3 + Co-autoria do [Template de atas](MINUTES/template.md)
- **Commit c66adffd71e208a0c4aca71fabdc66405ea8a927**:<br>Ata Reunião numero 4

### [REQ]
- **Issue #31**:<br>Criação de [Template de geração de User Stories](../../REQ/procedimento_criacao_requisitos.md) sobre o formato de issues
  
- **Issue #35**:<br>Criação de requisito sobre a forma de User Storie

- **Issue #61**:<br>Criação do [Ficheiro de requisitos](../../REQ/requirements_file.md.md) 

- **Issue #188**:<br>Verificação de requistos e anotação no pdf de requisitos disponibilizado pelo professor

- **Issue #186**:<br>Criação de [Tabela de Requisitos Feitos](../../REQ/done_requirements_file.md) para entregar á equipa Dash-It

### [TESTING]
- **Commit 580526b10abe7c17875c071ba202f30e9dd15106**:<br>Criação de script para testar requisito número 26 da equipa Dash-it

--- 
## Papel na Equipa

- Pertenci à equipa de Requisitos;
- Pertenci à equipa da Validação Independente;
