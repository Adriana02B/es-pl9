# David Leitão

@Kyndler

Developer
 
davidleitao@student.dei.uc.pt

Licenciatura em Engenharia Informática

---

My name is David Leitão, I'm a 20 year old student from Viana do Castelo and I'm looking forward to learning a bit more about what managing a project entails. I had no experience with Django prior to this class, but I'm looking forward to learning more about it.

---

## Contribuições 

### [DEV]
- Issue #111 (Abandonado eventualmente devido a sobreposição de funções)
- Issue #135
- Issue #162

--- 
## Papel na Equipa

- Membro da equipa de desenvolvimento
