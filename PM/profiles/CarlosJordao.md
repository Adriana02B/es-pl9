# Carlos Jordão

@OriginalNameTag

Developer

cecjordao@gmail.com

Licenciatura de Engenharia Informática

---

My name is Carlos Eduardo da Costa Jordão, I'm 21 years old and I program when it's required sometimes.
---

## Contribuições 

### [DEV]
- Issue #90

### [QA_Product]<br>
- Issue #218
- Ficheiro [avaliacao.md](QA_Process/Val-Indep/REQ01/avaliacaoReq1.md)
- Commit [ef5d1f6113c03b0734273c85a5d0b60b650179da]
<br>

- Issue #218
- Ficheiro [avaliacaoReq1.md](QA_Process/Val-Indep/REQ01/avaliacaoReq1.md)
- Commit [372773440adf56e61018d6992fc817bcdc507f8a]
<br>

- Issue #218
- Ficheiro [tabela-req1.md](QA_Process/Val-Indep/REQ01/tabela-req1.md)
- Commit [8187a5d5b9775e15013691f16064e7b32bd60115]
<br>

--- 
## Papel na Equipa<br>

Membro da equipa de desenvolvimento.

Membro da equipa de Validação Independente.
