# Margarida Souto
---
@MargaridaSouto19

Design

anasouto@student.dei.uc.pt

Licenciatura em Design e Multimédia - 3º ano

My name is Margarida Souto and I am 20 years old.

---
## Contribuições 

### [DES]
- Issue #89
- Issue #123
- Issue #124
- Commit 2a880196 - Documento de Apresentação da Equipa;
- Commit 2e4ad3eb - Mockup Active Members;
- Commit 001ee8c8 - Mockup Repository - Commits;
- Commit 76e06526 - Mockup Repository - Commits - History.

### [DEV]
- Issue #92
- Issue #93 
- Issue #94
- Commit 5ee645a3 - HTML - Project Information - Members;
- Commit 03a812ce - HTML - Repository - Active Members - Profile;
- Commit 4798e1e2 - CSS - Project Information - Members;
- Commit a3a50ef5 - CSS - Repository - Active Members - Profile .

--- 
## Papel na Equipa

- Fui TL da equipa de Design;
- Ajudei a equipa de Dev ao implementar a parte de HTML e CSS.
