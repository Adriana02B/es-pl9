# Francisco Faria
---

@faria2000

Tester

adfaria@student.dei.uc.pt

Licenciatura em Engenharia Informática - 3º ano

My name is Francisco Faria and I am 20 years old. 

---
### [PM]
- Realização da [ata da 7ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting07.md).

- Realização da [ata da 16ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting16.md).

- Revisão da [ata da 8ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting08.md).

- Revisão da [ata da 10ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting10.md).

- Revisão da [ata da 11ª reunião da equipa](MINUTES/REVIEWED/MinuteMeeting11.md).

### [REQ]<br>
- Issue #36 - Funcionalidade "back"

### [QA - Produto]<br>
- Issue #144

- Issue #142

- Issue #132
<br>

- Issue #183
- Ficheiro [avaliacao.md](QA_Process/Val-Indep/REQ 9/avaliacao.md)
- Commit [736c6ccfb2774745bf375b03668405eee29ccfd1]
<br>

- Issue #183
- Ficheiro [avaliacaoReq9.md](QA_Process/Val-Indep/REQ 9/avaliacaoReq9.md)
- Commit [72c49f085bcbc76f59fef7d8f288b3c7eda48fcd]
<br>

- Issue #183
- Ficheiro [tabelaReq9.md](QA_Process/Val-Indep/REQ 9/avaliacao.md)
- Commit [a6c218f40a135e67108283585a3d8f729c9d2625]
<br>

--- 
## Papel na Equipa<br>

 Pertenci à equipa de testes de produto
