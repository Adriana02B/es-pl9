# Hugo Ribeiro

@hrib

Requisitos
 
uc2007186621@student.uc.pt
 
Licenciatura em Design e Multimédia

---

Olá, o meu nome é Hugo e não percebo genuinamente nada disto. Help!

## Contribuições 


### [REQ]
- Criação de Requisito;
- Issue #40 Requisito: "Navegação livre entre as diversas vistas e itens de cada vista"

### [ARCH]

- Issue #75 : Requisito Produto #36
- Issue #79 : Requisito Produto #54
- Issue #80 : Requisito Produto #44
- Issue #213 
- Criação de Diagrama de Arquitetura : [Diagrama de Arquitetura](ARCH/Diagrama_de_Arquitetura.pdf);
- Desenvolvi Requisitos de produto;
- Criação da tabela de requisitos de produto [Tabela Reqs Produto](ARCH/Tabela Arch Requisitos.md);
- Atualização de tabela de requisitos de produto;
- Criação de Template e Tutorial de Requisitos de Produto: [Template e Tutorial Reqs Produto](ARCH/ReqProdutoTemplate.md).
--- 
## Papel na Equipa

Inicialmente pertencia à equipa de Requisitos e, duas semanas após integrei a equipa de Arquitetura.


