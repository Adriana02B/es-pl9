# Luís Santos

@Iuissantos

Process Quality Assurance 

luissantos@student.dei.uc.pt

Licenciatura em Engenharia Informática

---

Luis, 20 years

## Contribuições 

### [QA - Produto]
- Issue #164
- Issue #207

### [QA - Processo]
- Ficheiro [QA_Process/ApreciacaoGlobalProcesso.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/QA_Process/ApreciacaoGlobalProcesso.md)
- Ficheiro [QA_Process/DiagramaProcesso.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/QA_Process/DiagramaProcesso.md)
- Ficheiro [QA_Process/RelatorioProcessoEquipa.md](https://gitlab.com/Adriana02B/es-pl9/-/blob/main/QA_Process/RelatorioProcessoEquipa.md)


### [ATAS]
- Redação da ata nº 2 


--- 
## Papel na Equipa
- TL da equipa de integrção na fase inicial do Projeto;
- Pequena ajuda na equipa de testing
- Membro da equipa de QA - Processo;
