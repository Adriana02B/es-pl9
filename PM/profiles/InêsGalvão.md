#Inês Galvão

@igalvao13
Design
inesgalvao@student.dei.uc.pt
Licenciatura em Design e Multimédia - 3º ano
My name is Inês Galvão and I am 20 years old.


Contribuições

[DES]

Issue #69

Issue #81

Issue #75

Commit cf8f76cb - Logotipo GitNub;
Commit 56df9d68 - Mockup Repository - Author;
Commit 7b91658a - Mockup Repository - Files - File;
Commit 56df9d68 - Mockup Repository - Author - Files.


[DEV]

Issue #69

Issue #65

Issue #73

Commit 818bf5cc - HTML - Login;
Commit 51453c5b - HTML - Issue;
Commit 4b78dca7 - CSS - Login;
Commit f9ab1509 - CSS - Issue .



Papel na Equipa

Fui membro da equipa de Design;
Ajudei a equipa de Dev ao implementar a parte de HTML e CSS.
