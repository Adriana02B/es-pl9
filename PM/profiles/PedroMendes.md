# Pedro Miguel Branco Mendes

@Pedroooo

Architect

pedrombrancomendes@gmail.com

LEI

--------

I like to program in Java and Python. I also have some HTML and CSS knowledge.

--------

I am 20 years old and I love everything about comics, fantasy worlds, wargames, miniatures, etc.

## Contribuições 


### [REQ]
- Criação de Requisito;
- Issue #38 Requisito: "Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações."

### [ARCH]

- Issue #110 : Requisito Produto: #45
- Issue #73 : Requisito Produto: #50
- Issue #63 : Requisito Produto: #38

- Criação de Diagrama de Arquitetura : [Diagrama de Arquitetura](ARCH/Diagrama_de_Arquitetura.pdf);
- Desenvolvi Requisitos de produto, Issues: #110, #73, #63;
- Criação da tabela de requisitos de produto [Tabela Reqs Produto](ARCH/Tabela Arch Requisitos.md);
- Atualização de tabela de requisitos de produto;
- Criação de Template e Tutorial de Requisitos de Produto: [Template e Tutorial Reqs Produto](ARCH/ReqProdutoTemplate.md).
--- 
## Papel na Equipa

Born and raised an architect. Pertenci à equipa de arquitetura desde o início, elaborando juntamente com os meus colegas todos os seus documentos.
