# Guilherme Sousa de Oliveira e Cruz Junqueira

@Guilherme20

Testers/Quality 
 
junqueiraguilherme@hotmail.com

Engenharia Informatica

---

Nascido em Coimbra mas viajei pelo mundo conhecendo um pouco de tudo.

---

## Contribuições

[QA-Processo]

-Issue #141
-Issue #150
-Issue #151 

[Validação independente]

-Issue #189


### ATAS

Criação e envio da Ata Nº1 24/09/2021
Revisão das atas: Ata Nº2  01/10/2021 
                  Ata Nº11 17/11/2021
                  Ata Nº15 01/12/2021

---
### Papel na Equipa

-Pertenci a equipa de testing/quality e nas ultimas duas semanas pertecia a equipa da Validação Independente.
