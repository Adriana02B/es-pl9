# Descrição do processo de trabalho da Equipa GitNub

### Após a receção do documento com o que é pedido pela empresa, a equipa passa por vários processos pela seguinte ordem:

- **Gestores de Projeto**
  - Equipa de Gestão de Projeto: Adriana Bernardo e João Vaz;
    - Gestão de recursos da equipa;
    - Criação de documentos de organização;
- **Requisitos**
  - Equipa de Requisitos: <ins>Pedro Henriques</ins> e Pedro Chaves;
    - Criação e verificação de User Stories;
    - Organização de requisitos;
- **Equipa de Arquitetura**
  - Equipa de Arquitetura: Pedro Mendes, Bernardo Arzileiro e Hugo Ribeiro;
    - Criação a partir dos User Stories verificação de requisitos de produto;
    - Organização de requisitos de produto em endpoints;
- **Qualidade de Processo**
  - Equipa de Qualidade de Processo: Eva Simões e Luís Santos;
    - Verificação do que é produzido pelas outras equipas, ex.: documento de requisitos;
- **Qualidade de Produto (Testes)**
  - Equipa de Qualidade de Produto (Testes): <ins>Marta Santos</ins>, Francisco Faria, Guilherme Junqueira, Iago Bebiano, Maria Sarmento e Tiago Ventura;
      - Criação e testagem de cada um dos requistos;
- **Design**
  - Equipa de Design: <ins>Margarida Souto</ins>, Inês Galvão e Maria Raposo
    - Desenvolvimento de código CSS e HTML;
    - Criação de mockups;
- **Desenvolvimento**
  - Equipa de Desenvolvimento: <ins>Rodrigo Machado</ins>, Carlos Jordão, David Leitão, Gonçalo Folhas, Rui Bernardo, João Fernandes e Samuel Pires;
    - Desenvolvimento de código backend;
    - Sempre que um código não passa nos testes, volta para a equipa de desenvolvimento;
- **Integração e Deploy**
  - Não existe uma equipa para este processo, sendo desempenhado pelo líder da equipa de desenvolvimento, Rodrigo Machado;
    - Faz a integração e o deploy do que foi desenvolvido até ao momento;
