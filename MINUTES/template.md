# Ata de Reunião nº X

---
Departamento de Engenharia Informática, dia X de mês X (extenso) de 2021

**Equipa:** Nome equipa

**Duração:** Tempo

**Presenças:**

- 
-
-
-
-
...

**Ausências:**

-
-
-
-
-
...

---

## Sumário da reunião:

Escrever aqui


---
## Assuntos tratados:

Escrever aqui

---
## Assuntos pendentes:

Escrever aqui

---

#### Ata redigida por: Nome1; Nome2
#### Ata revista por: Nome3; Nome4
