# Ata de Reunião nº 13
---
Discord de Equipa, dia 24 de Novembro de 2021

**Equipa:** GitNub

**Duração:** 1 hora

**Presenças:**
- Bernardo Arzileiro
- Carlos Jordão
- Duarte Henriques
- Eva Simões
- Francisco Faria
- Gonçalo Folhas
- Guilherme Junqueira
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Luís Santos
- Margarida Souto
- Maria Raposo
- Maria Sarmento
- Pedro Chaves
- Pedro Mendes
- Rodrigo Machado
- Rui Bernardo
- Samuel Pires

---
## Sumário da reunião:
Esta reunião baseou-se em mostrar o que foi feito e também em falar acerca de alguns temas importantes para o desenvolvimento do nosso projeto que irão estar descritos mais pormenorizadamente nos assuntos tratados.

---
## Assuntos tratados:

Começámos por mostrar o que tinha sido feito, como por exemplo a organização dos issues em tabelas e, também, a tabela de Issues por Requisitos. Também foi abordado o tema das Views mas estas ainda se encontram em desenvolvimento.

O Project Manager, João Vaz, pediu para darmos início à organização do nosso perfil do GitLab de acordo com os issues realizados, como foi pedido pelo professor, e também para receber mais feedback semanal por parte de todos. Pediu à equipa de requisitos para organizar os requisitos de acordo com a sua prioridade. 

Voltou-se a falar no assunto da landing page mas desta vez de uma forma mais elaborada, sem ser apenas uma página inicial de "Comming Soon...".

Por último, foi falada a possibilidade de participação no trabalho em outras partes do projeto às pessoas que se encontram com o trabalho mais estagnado e, foi recomendado a cada um aprender sobre a pipeline e sobre testing em Django(mais para a equipa de testes).

---
## Assuntos pendentes:

- Pesquisa sobre pipeline e Django.

---

#### Ata redigida por: Maria Sarmento
#### Ata revista por: João Vaz, Pedro Mendes, Pedro Chaves
