# Ata de Reunião nº 7
---
Departamento Engenharia Informática, dia 05 de novembro de 2021

**Equipa:** GitNub

**Duração:** 120 minutos

**Presenças:**
- Bernardo Arzileiro
-	Duarte Henriques
-	David Leitão
-	Francisco Faria
-	Gonçalo Folhas
-	Hugo Ribeiro
-	Iago Bebiano
-	Inês Galvão
-	João Fernandes
-	João Vaz
-	Maria Sarmento
-	Marta Santos
-	Pedro Chaves
-	Pedro Mendes
-	Rodrigo Machado
-	Rui Bernardo
-	Samuel Pires

**Ausências**
- Adriana Bernardo
- Carlos Jordão
- Eva Simões
- Guilherme Junqueira
- Luís Santos
- Samuel Pires
- Maria Raposo
- Tiago Ventura


---
## Sumário da reunião:
Análise do trabalho realizado até ao momento, o qual não satisfaz o professor levando a este oferecer várias guidelines para ajudar a equipa a avançar.
“Cliente” exige um ecrã com lista dos membros, divisão de tarefas para a realizar.

---
## Assuntos tratados:

Professor oferece alguns guidelines para ajudar a equipa a avançar:
- Realizar uma primeira versão simples onde apenas aparecem os nomes dos membros;
-	pyDriller (correrá no servidor para ir buscar informações em git) e como este interage com o Django e com GitLab;
-	Recomenda a utilização de uma database;
-	Criar um script que corre e mantem um clone local do git atualizado;
	 - (1ª fase) botão que atualiza clone local;
     - (2ª fase) Usar “look” que substitui o botão, automatizando o processo.
-	Interação constante com o “cliente” de modo a ser Agile;
-	Guardar projeto em pastas e sub-pastas de modo a evitar problemas de grande escala;
-	Nunca afastar muito do branch principal;

Project Manager Rodrigo Machado mostra código template criada por ele e envia para a equipa.


---
## Assuntos pendentes:

**DEADLINE 12 NOVEMBRO:** Ecrã com a lista dos membros da equipa preparado para a próxima reunião com o “cliente”:
-	Developers devem decidir que membros ficam encarregues;
pyDriller fornece praticamente tudo, menos os issues. Necessário encontrar outro/os APIs para essa funcionalidade.




---

#### Ata redigida por: Iago Bebiano e Francisco Faria
#### Ata revista por: Gonçalo Folhas e Marta Santos
