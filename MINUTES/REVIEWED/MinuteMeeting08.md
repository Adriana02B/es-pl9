# Ata de Reunião nº 8
---
Departamento Engenharia Informática, dia 12 de novembro de 2021

**Equipa:** GitNub

**Duração:** 120 minutos

**Presenças:**
- Adriana Bernardo
- Bernardo Arzileiro
- Duarte Henriques
-	Hugo Ribeiro
-	Iago Bebiano
-	Inês Galvão
-	João Fernandes
-	João Vaz
-	Maria Sarmento
-	Marta Santos
-	Rodrigo Machado
- Maria Raposo
-	Samuel Pires
- Guilherme Junqueira Discord
- Eva Simões Discord
- Luís Santos Discord
- David Leitão Discord
- Gonçalo Folhas Discord
- Rui Bernardo Discord
- Pedro Mendes Discord

**Ausências**
- Carlos Jordão
- Tiago Ventura
- Pedro Chaves
- Francisco Faria

---

## Sumário da reunião:

Tratamento de assuntos urgentes para garantir o avanço do projeto.

Divisão de tarefas.

Marcação de reuniões semanais (Quarta 15h).

---
## Assuntos tratados:
Demonstração de novas funcionalidade ao "cliente" cumprindo a DEADLINE imposta na semana anterior.

Rodrigo (PM) fica encarregue do deploy.

Equipa de desenvolvimento trabalha no branch development.

João Vaz (TL) não precisa mexer no código, apenas verificar se faz sentido e se está correto antes de se dar o merge no branch development, sendo necessário a revisão do mesmo por parte de mais membros.

Pessoal de testes deve criar/pensar em testes antes de existir algo para testar.

Antes de encerrar um issue, este deve passar pelos arquitetos.

Rodrigo forneceu guidelines sobre templates criadas por ele, de  modo a facilitar trabalho futuro.

Designers também podem ajudar no CSS.

Marcação de reunião em horário extra aula (Quarta 15h).

---

## Assuntos pendentes:

Urgente começar a trabalhar nos requisitos.





---

#### Ata redigida por: Iago Bebiano e João Vaz
#### Ata revista por: Samuel Pires e Francisco Faria
