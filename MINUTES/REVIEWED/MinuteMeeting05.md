# Ata de Reunião nº 5

---
Departamento de Engenharia Informática, dia 15 do mês Outubro de 2021

**Equipa:** GitNub

**Duração:** 2h

**Presenças:**

- Adriana Bernardo  [@Adriana02B](https://gitlab.com/Adriana02B)
- Bernardo Arzileiro [@BennyAltF4](https://gitlab.com/BennyAltF4)
- David Leitão [@Kyndler](https://gitlab.com/Kyndler)
- Francisco Faria [@faria2000](https://gitlab.com/faria2000)
- Gonçalo Folhas [@Folhas](https://gitlab.com/Folhas)
- Guilherme Junqueira [@Guillherme20](https://gitlab.com/Guillherme20)
- Hugo Ribeiro [@hrib](https://gitlab.com/hrib)
- Iago Bebiano [@iagosilvabebiano](https://gitlab.com/iagosilvabebiano)
- Inês Galvão [@igalvao13](https://gitlab.com/igalvao13)
- João Fernandes [@JoaoJoaoFernandes](https://gitlab.com/JoaoJoaoFernandes)
- João Vaz [@Vaz07](https://gitlab.com/Vaz07)
- Margarida Souto [@MargaridaSouto19](https://gitlab.com/MargaridaSouto19)
- Maria Raposo [@mariafraposo](https://gitlab.com/mariafraposo)
- Maria Sarmento [@sarmento13](https://gitlab.com/sarmento13)
- Marta Santos [@marta-c-santos](https://gitlab.com/marta-c-santos)
- Pedro Chaves [@PedrorChaves](https://gitlab.com/PedrorChaves)
- Pedro Henriques [@pedroduartesh](https://gitlab.com/pedroduartesh)
- Pedro Mendes [@Pedroooo](https://gitlab.com/Pedroooo)
- Rodrigo Machado [@bitl0ck](https://gitlab.com/bitl0ck)
- Samuel Pires [@ItzPires](https://gitlab.com/ItzPires)
- Tiago Ventura [@Venturazz](https://gitlab.com/Venturazz)

**Ausências:**

- Carlos Jordão [@OriginalNameTag](https://gitlab.com/OriginalNameTag)
- Eva Simões [@eva_simoes](https://gitlab.com/eva_simoes)
- Luis Santos [@Iuissantos](https://gitlab.com/Iuissantos)
- Rui Bernardo [@ruibernardo521](https://gitlab.com/ruibernardo521)

---

## Sumário da reunião:

Realizada revisão do template de requisitos e de alguns requisitos

Discutido o processo de arquitetura, testes e de qualidade


---
## Assuntos tratados:

Iniciamos a reunião com o docente a realizar uma revisão ao processo e template dos requisitos e ainda de alguns requisitos

De seguida o docente começou por explicar o processo de arquitetura

Ainda foi discutido o processo de qualidade de produto, testes e devs

Em relação aos Testes, foi aconselhado utilizar o “ODC” para carimbar, classificar e mostra os defeitos mais comuns de cada developer apresentando uma estatística no seu perfil de modo a evitar repetir esses defeitos

Sobre a qualidade de produto, foi recomendado a automatização dos processos 

Em todos os tópicos, o docente deu sugestões para a realização do trabalho

---
## Assuntos pendentes:

- Validação dos requisitos
- Servidor e Página web

---

#### Ata redigida por: Pedro Chaves; Samuel Pires
#### Ata revista por: Gonçalo Folhas; Pedro Mendes
