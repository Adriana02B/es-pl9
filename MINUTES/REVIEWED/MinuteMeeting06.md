# Ata de Reunião nº 6
---
Discord de Equipa, dia 20 de Outubro de 2021

**Equipa:** GitNub

**Duração:** 35 minutos

**Presenças:**
- Bernardo Arzileiro
- David Leitão
- Duarte Henriques
- Eva Simões
- Francisco Faria
- Gonçalo Folhas
- Guilherme Junqueira
- Pedro Mendes
- Inês Galvão
- João Fernandes
- João Vaz
- Luís Santos
- Maria Sarmento
- Marta Santos
- Pedro Chaves
- Rodrigo Machado
- Rui Bernardo
- Samuel Pires

---
## Sumário da reunião:
Nesta reunião decidimos o logótipo da equipa, procurámos começar a desenvolver uma landing page e ficámos de pesquisar uma plataforma cooperativa para o Kanban board.

---
## Assuntos tratados:

Começámos por debater o protótipo de logótipo proposto pela parte de Design, o qual foi aprovado sem oposições. 

O Project Manager, Rodrigo Machado, propôs a junção de mais uma pessoa ao papel de Arquiteto de modo a suavizar o trabalho deste, ficando assim duas pessoas com este cargo. Este novo Arquiteto seria o Hugo Ribeiro (atualmente em Requirements), que não teve a possibilidade de estar presente e, por isso, este ponto ficou pendente.

Como próximo objetivo, definimos o desenvolvimento de uma landing page apenas com uma mensagem de “Coming soon…” , cujo mockup será proposto também pela equipa de Design.

Por último, foi recomendado a cada um aprender sobre o Kanban board, de modo a facilitar trabalho futuro.

---
## Assuntos pendentes:

- Procura de plataforma para Kanban board.

---

#### Ata redigida por: Maria Sarmento
#### Ata revista por: Rodrigo Machado e Iago Bebiano

