# Ata de Reunião nº 4

---
Departamento de Engenharia Informática, dia 8 de mês Outubro de 2021

**Equipa:** GitNub

**Duração:** 2 horas

**Presenças:**

- Adriana Bernardo
- Bernardo Arzileiro 
- David Leitão
- Eva Simões
- Francisco Faria
- Gonçalo Folhas
- Hugo Ribeiro
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Luis Santos
- Margarida Souto
- Maria Raposo
- Marta Santos
- Pedro Chaves
- Pedro Henriques
- Pedro Mendes
- Rodrigo Machado
- Rui Bernardo
- Samuel Pires
- Tiago Ventura

**Ausências:**
- Carlos Jordão
- Guilherme Junqueira
- Maria Sarmento

---

## Sumário da reunião:

Na reunião foi estabelecido o nome de equipa.

Foi concluída a tarefa de criação de perfis.

Foi discutido o modo de tratamento dos requisitos. 


---
## Assuntos tratados:

Começámos a reunião por analisar o trabalho feito até à data e finalizámos a criação dos perfis em falta, foram também fechados os issues pendentes dos perfis.

O docente fez uma revisão da aula teórica anterior acerca dos requisitos, pois a equipa mostrou algumas dificuldades quanto a esta função.

Foi discutido com o professor a organização da equipa e o modo como vamos processar os requisitos e a sua importância.
Falámos de alguns requisitos que vão ser necessários para a próxima reunião e o modo como os vamos tratar (ver readme de REQ). 

De forma a perceber o que iremos fazer nas próximas aulas discutimos também as funções do arquiteto, já que estas serão particularmente relevantes futuramente.

No final da reunião foi realizada uma votação para decidir o nome de equipa.


# Nomes de Equipa - Votação

- GitNub - 7 votos
- GitGud - 5 votos
- desESperados - 4 votos
- BDC - 4 votos
- ESplanada - 1 votos
- dESpertos - 1 votos

Faltavam 3 pessoas para a votação que não compareceram à reunião.

---
## Assuntos pendentes:

Lista de requisitos para a próxima reunião de aula.







---

#### Ata redigida por: João Fernandes, Pedro Henriques
#### Ata revista por: Adriana Bernardo, Eva Simões , Maria Sarmento, João Vaz
