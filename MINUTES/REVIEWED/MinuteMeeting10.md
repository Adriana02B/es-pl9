# Ata de Reunião nº 10
---
Discord de Design, dia 16 de novembro de 2021

**Equipa:** GitNub

**Duração:** 30 minutos

**Presenças:**
* Inês Galvão
* Maria Raposo
* Margarida Souto

**Ausências**

---
## Sumário da reunião:
    Divisão das tarefas a realizar na semana de 15 a 22 de novembro.

---
## Assuntos tratados:
	- Análise dos requisitos presentes no gitlab;
    - Divisão de tarefas (Mockups):
    - Margarida Souto: Mockup ‘Lista de membros ativos’ (#43, #51, #35, #53) e Mockup ’Project Information’ (#39);
    - Inês Galvão:  Mockup ‘Lista de ficheiros’ (#38, #49, #42, #54, #50) e Mockup ‘Lista de Requisitos’ (#33, #34, #37);
    - Maria Raposo:  Manual de Normas e Mockup ‘Landing Page’ (menu + #32).
    - Mais mockups irão ser adicionados às tarefas, dependendo do trabalho realizado pelo departamento de Arquitetura.




---
## Assuntos pendentes:
	Nenhum assunto ficou pendente no final desta reunião.

---

#### Ata redigida por: Inês Galvão
#### Ata revista por: Eva Simões, João Vaz, Rui Bernardo e Francisco Faria
