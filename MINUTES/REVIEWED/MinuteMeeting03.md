# Ata de Reunião nº 3
---
Discord de Equipa, dia 03 de Outubro de 2021

**Equipa:** PL9 (temporário)

**Duração:** 2 horas e 30 minutos

**Presenças:**
- Adriana Bernardo
- João Fernandes
- Pedro Henriques

---
## Sumário da reunião:
Nesta reunião inicializou-se a organização do repositório.

De forma a facilitar e uniformizar o processo de escrita de atas, foi criado um template.

---
## Assuntos tratados:

Seguindo o esquema que nos foi enviado pelo professor da cadeira, criámos uma primeira versão de estrutura de repositório de equipa no GitLab. Foi também criada uma nova pasta, contendo as atas das reuniões de equipa, dividas em duas categorias, atas por rever e atas revistas.

Verificámos que, nas últimas duas atas, o esquema adotado pela equipa diferiu, posto isto, foi criado um guia que irá ser utilizado na criação das atas seguintes, desta forma, esperamos conseguir uniformizar a estrutura das mesmas, para isso adotamos a linguagem MarkDown, pela sua simplicidade e integração com o GitLab, plataforma usada pela equipa.

Tentamos identificar alguns assuntos que devem ser tratados com a presença de um número substancial de membros, para resolver até ou durante uma próxima reunião.

---
## Assuntos pendentes:

- Nome de equipa
- Idioma das atas
- Idioma do projeto
- Decisão de quem redige as atas (Pessoa/s única/s ou Rotação)
- Tanto o esquema do repositório como das atas de reunião pode estar sujeito a alterações
- Criar local na web como pedido pelo docente
- Escolher estilo de nomeclatura de ficheiro a adotar no repositório
- Escolher convenções de estilo a utilizar no código

---

#### Ata redigida por: Pedro Henriques
#### Ata revista por: João Fernandes
