# 1ª Reunião PL9 ES 
 
 Departamento de Engenharia Informática, 24/09/2021 Sala C6.3 

---

## Assuntos tratados

Introdução ao material que será utilizado ao longo do percurso do projeto (Python e Django bem como GitLab para comunicação e comparação de trabalho feito, caso tenham dúvidas, é recomendado a visualização de vídeo do Youtube).

Utilização de Slack/Discord para comunicação entre o grupo.

Definição dos diversos papeis que cada elemento do grupo terá que fazer ou focar se (Gestor técnico, gestor de qualidade, gestor de requisitos, gestor de produto, suporte técnico (irá olhar para a infraestrutura de um ponto de vista do cliente), etc).

Registação da página pessoal de GitLab pelos vários membros do projeto.

Demonstração dos diversos passos que um ticket tem que percorrer para que seja resolvida.

Distribuição de cargos:
 - Registers: Pedro Henriques
 - Product Manager: Adriana Bernardo
 - Team Leaders: Samuel Pires, Pedro Mendes, João Fernandes, Pedro Chaves
 - Design team: Maria Raposo, Inês Galvão, Margarida Souto
 - Development team: Gonçalo Folhas, João Vaz, Rodrigo Machado, Carlos Jordão, David Leitão, Rui Bernardo
 - Test team: Marta Santos, Francisco Faria, Maria Sarmento
Quality team: Guilherme Junqueira, Eva Simões, Iago Bebiano, Tiago Ventura
 - Integration Team: Bernardo Arzileiro, Luís Santos

#### Ata redigida por: Guilherme Junqueira

#### Ata revista por: Equipa
 