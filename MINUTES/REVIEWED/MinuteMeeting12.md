# Ata de Reunião nº 12
---
Departamento de Engenharia Informática, dia 19 de novembro de 2021

**Equipa:** GitNub

**Duração:** 120 minutos

**Presenças:**
- Duarte Henriques
- Gonçalo Folhas
- Guilherme Junqueira
- Rodrigo Machado
- Adriana Bernardo
- Eva Simões
- Francisco Faria
- Iago Bebiano
- Inês Galvão
- Margarida Souto
- Maria Raposo
- Maria Sarmento
- Marta Santos
- Samuel Pires
- Bernardo Arzileiro
- David Leitão

**Ausências**
- Carlos Jordão
- João Fernandes
- João Vaz (DDN)
- Pedro Mendes
- Hugo Ribeiro
- Luís Santos  
- Pedro Chaves  
- Rui Bernardo (confinamento)
- Tiago Ventura(confinamento)

---
## Sumário da reunião:

Verificação do trabalho feito desde a última reunião.
Atualização da ordem da cadeia de trabalho.
Conversa com a equipa GitNub sobre o que foi feito até ao momento e o que ainda falta fazer.
Esclarecimento quanto às datas de entrega e explicação do trabalho a ser feito.


---
## Assuntos tratados:

Professor relembra sobre os prazos limites das datas.
Quis ver os mockups.
Fala sobre o trabalho dos arquitetos e sobre a necessidade dos mesmos.
Propõe troca da tabela em excel para gitlab.
Atribuir prioridades aos requisitos, testando primeiro os com maior prioridade.

Deve existir alguém apenas responsável pela pipeline.

Verificação = testes -- Validação = verificar se requisitos foram cumpridos;

Gestor de projetos deve ser capaz de verificar o ponto de situação da equipa, de modo a poder gerir e escolher que requisitos são possivéis realizar com o tempo disponivél.

Equipa de development criou um back button e o "cliente" questiona a utilidade do mesmo sabendo que não existem vistas e testes que se possam realizar sobre o mesmo.

Rodrigo (TL) é o "deploy master", sendo que é ele que tem acesso direto à máquina virtual;

#### Ata redigida por: Gonçalo Folhas e Iago Bebiano
#### Ata revista por: David Leitão, Eva Filipe, Adriana Bernardo
