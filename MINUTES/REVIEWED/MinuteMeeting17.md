# Ata de Reunião nº 17
---
Departamento de Engenharia Informática, dia 10 de Dezembro de 2021 pelas 11h

**Equipa:** GitNub

**Duração:** 2 horas

**Presenças:**
- Adriana Bernardo
- Bernardo Arzileiro
- David Leitão
- Duarte Henriques
- Gonçalo Folhas
- Guilherme Junqueira
- Hugo Ribeiro
- Inês Galvão
- João Fernandes
- João Vaz
- Luís Santos
- Maria Raposo
- Maria Sarmento
- Margarida Souto
- Marta Santos
- Pedro Chaves
- Pedro Mendes
- Rodrigo Machado
- Rui Bernardo 
- Samuel Pires
- Tiago Ventura


**Ausências:**
- Carlos Jordão
- Eva Simões (DDN)
- Francisco Faria
- Iago Bebiano



---
## Sumário da reunião:

Apresentação do produto entregue no dia 4 (Validação Independente).
Explicação sobre o funcionamento da equipa.
Atividade individual de cada membro da equipa.

---
## Assuntos tratados:

Apresentação do produto da equipa que continha as seguintes funções:
    Landing page;
    Lista de Membros;
    Lista de commits de cada membro;
    Árvore de ficheiros;
    Lista de issues (abertos, activos e total);
    Funcionalidade "back" (Com alguns problemas);

O professor fala com as diversas equipas para saber como estas estão a funcionar e como estão a interagir entre si.
Refere que um dos maiores problemas da equipa é o facto de cada equipa saber pouco sobre o trabalho realizado pelas restantes equipas, como se cada equipa trabalhasse em silos. 
O professor comenta a atividade de cada membro ao longo deste semestre, segundo as informações apresentadas no GitLab.

## Nota: 

Não devemos atualizar o produto entregue na Validação independente até ao dia 17 (dia de apresentação ao cliente), pois as novas funcionalidades poderão influenciar os testes da equipa que ficou com o produto.

---

#### Ata redigida por: Gonçalo Folhas e Rui Bernardo
#### Ata revista por: Adriana Bernardo, Eva Simões e Marta Santos
