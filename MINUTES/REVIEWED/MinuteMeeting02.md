# 2ª Reunião PL9 ES 

Departamento de Engenharia Informática, 01/10/2021 - 11:00 Sala C6.3

--- 

**Presenças:**
- Adriana Bernardo
- Bernardo Arzileiro 
- David Leitão
- Eva Simões
- Francisco Faria
- Gonçalo Folhas
- Hugo Ribeiro
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Luis Santos
- Margarida Souto
- Marta Santos
- Pedro Chaves
- Pedro Henriques
- Pedro Mendes
- Rodrigo Machado
- Rui Bernardo
- Samuel Pires
- Tiago Ventura
- Carlos Jordão
- Guilherme Junqueira
- Maria Sarmento

**Ausências:**
- Maria Raposo

---

## Assuntos tratados:
Revisão  e adição dos papéis que cada elemento terá a desempenhar  sendo que estes não serão fixos podendo ainda sofrer alterações mais para a frente.
	
O Rodrigo Machado (r.mota.machado@gmail.com) passou para Product Manager pois achamos necessário a presença de mais um elemento nesse cargo.
	
Foi adicionado o cargo de Software Architect que vai ser executado pelo Pedro Mendes (pedrombrancomendes@gmail.com). Cargo que vai ser essencial pois é este que vai receber os requesitos e tomar as decisões de como os implementar.
	
Quanto ao cargo de Quality Assurance (QA) ficou separado em 2 divisões, QA do processo e QA do produto. 
	
QA do processo vai ficar responsável pela verificação do funcionamento do grupo como equipa, confirmando que esta colabora e comunica entre si. Sendo a Eva Simões (evafilipefs@gmail.com) o elemento escolhido para este cargo.
	
QA do poduto vai verificar se o código que lhes chega dos Developers está devidamente percetível, limpo,… Sendo que tais condições não se verifiquem este seja reencaminhado de volta para os mesmos. Neste cargo vão se manter os elementos presentes anteriormente nos Testers tendo a Marta Santos (martacsantos01@gmail.com) como team leader.
	
Foi nos pedido, pelo docente, para criar um local na web (página, site) onde vamos colocar a informação da equipa. 

---
#### Ata redigida por: Bernardo Arzileiro, Rui Bernardo, Luis Santos

#### Ata revista por: Guilherme Junqueira