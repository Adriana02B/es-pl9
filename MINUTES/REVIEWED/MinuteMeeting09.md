# Ata de Reunião nº 9
---
Discord de Equipa, dia 14 de novembro de 2021

**Equipa:** GitNub

**Duração:** 20 minutos

**Presenças:**
- Adriana Bernardo
- Bernardo Arzileiro
- Carlos Jordão
- Duarte Henriques
- Eva Simões
- Fransico Faria
- Gonçalo Folhas
- Guilherme Junqueira
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Luís Santos
- Margarida Souto
- Maria Sarmento
- Marta Santos
- Pedro Chaves
- Pedro Mendes
- Rui Bernardo
- Samuel Pires
- Tiago Ventura

**Ausências**
- David Leitão
- Hugo Ribeiro
- Maria Raposo
- Rodrigo Machado

---
## Sumário da reunião:
		Explicação das diferentes decisões tomadas pelos PMs, desde a mudança de um dos PMs até às DEADLINES recomendadas.
		Criação de uma lista com as diferentes tarefas a serem feitas por cada membro do grupo.
		Mudanças no documento feito pela equipa de Design.

---
## Assuntos tratados:
	Mudança de PMs, o PM anterior, Rodrigo Machado, trocou de lugar com o TL dos Dev , João Vaz.
	Explicação por parte dos PMs das DEADLINES impostas, mudanças feitas no grupo do discord (para facilitar a comunicação entre equipas) e do trabalho a ser feito por cada equipa.
    Definiu-se o processo que a equipa terá de percorrer apartir de agora:
        1. São criados os requisitos, e aprovados;
        2. A equipa de arquitetura avança;
        3. Seguidamente, avança a equipa de desenvolvimento e a equipa de design;
        4. Após o trabalho destas equipas, a equipa de testes e qualidade trabalham sobre o que foi desenvolvido;
        5. A equipa de integração, junta tudo no final;
        6. É dado o deploy;
        7. E o processo repete-se de modo sequencial.
	Equipa de Arquitetura terá que definir as views até quarta-feira, limite máximo, sendo que esta equipa vai tentar ter as coisas prontas antes.
	Equipa de Desenvolvimento, Design e Testes irão começar a trabalhar quando o trabalho dos Arquitetos estiver concluido.
	Tendo a equipa de Design e Desenvolvimento até quinta-feira para entregar o trabalho feito.	
	Criação de um documento pela PM Adriana Bernardo com as diferentes atividades feitas, ou a ser realizadas no momento, por cada elemento do grupo, em que todos os elementos têm acesso.
    Definimos, ainda, que sempre que um membro da equipa avançar num passo avisa os PMs para que a equipa consiga avançar para as seguintes etapas e chegar mais rapidamente até ao fim.
	Propostas de algumas mudanças no documento feito pela equipa de Design. 

---
## Assuntos pendentes:
	Nenhum assunto ficou pendente no final desta reunião.

---

#### Ata redigida por: Rui Bernardo e Adriana Bernardo
#### Ata revista por: Maria Sarmento, Marta Santos, Gonçalo Folhas e Eva Simões
