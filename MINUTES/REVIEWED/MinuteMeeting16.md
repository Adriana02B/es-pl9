# Ata de Reunião nº 16
---
Departamento de Engenharia Informática, dia 03 de Dezembro de 2021

**Equipa:** GitNub

**Duração:** 2 horas

**Presenças:**
- Adriana Bernardo
- Bernardo Arzileiro
- David Leitão
- Duarte Henriques
- Francisco Faria
- Gonçalo Folhas
- Guilherme Junqueira
- Hugo Ribeiro
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Maria Raposo
- Marta Santos
- Pedro Chaves
- Pedro Mendes
- Rodrigo Machado
- Samuel Pires
- Maria Sarmento
- Margarida Souto

**Ausências:**
- Eva Simões
- Luís Santos
- Rui Bernardo (DDN)
- Carlos Jordão
- Tiago Ventura


---
## Sumário da reunião:

Foco na entrega de dia 4 de Dezembro.


---
## Assuntos tratados:

O Professor esclareceu dúvidas relativamente à entrega da Validação Independente e falou individualmente com cada grupo que se encontrava a trabalhar nas diversas tarefas, de modo a preparar a entrega de dia 4.

O Professor disse que os testes tem de estar mais “próximos” do código (implementação/execução).

A equipa começará a trabalhar a partir de dia 5 na Validação Independente do trabalho da outra equipa.




---

#### Ata redigida por: Iago Bebiano e Francisco Faria
#### Ata revista por: João Vaz, Adriana Bernardo e Eva Simões
