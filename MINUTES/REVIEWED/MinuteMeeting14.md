# Ata de Reunião nº 14
---
Departamento de Engenharia Informática, dia 26 de Novembro de 2021

**Equipa:** GitNub

**Duração:** 2 horas

**Presenças:**
- Bernardo Arzileiro
- David Leitão
- Duarte Henriques
- Francisco Faria
- Gonçalo Folhas
- Guilherme Junqueira
- Hugo Ribeiro
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Margarida Souto
- Maria Raposo
- Maria Sarmento
- Marta Santos
- Pedro Chaves
- Pedro Mendes
- Rodrigo Machado
- Rui Bernardo
- Tiago Ventura
- Samuel Pires

**Ausências:**
- Adriana Bernardo
- Carlos Jordão
- Eva Simões
- Luís Santos

---
## Sumário da reunião:
Nesta reunião foi apresentado ao cliente o que foi feito durante esta última semana e cada equipa tirou dúvidas sobre as diferentes tarefas a realizar.

---
## Assuntos tratados:

Começamos por apresentar a lista de membros (feita pelo Rodrigo Machado), a lista de commits (feita pelo Gonçalo Folhas em conjunto com o João Fernandes) e o repositório (feito pelo Samuel Pires). 

Conversa com as equipas de Developing e Design sobre a maneira de como estão a ser tratados os requisitos e de como estas estão a comunicar entre si, com a equipa de Quality/Testing sobre como os testes irão ser feitos, como os começar, sobre os requisitos a serem desenvolvidos e sobre o pipeline e ainda com os Arquitetos sobre o trabalho feito, deve ser focado trabalho mais simples em vez de estar a ser desenvolvido trabalho mais complexo.

Explicação sobre o processamento de requisitos através de vários issues (um por categoria) e uma consolidação sobre processos de trabalho.

---
## Assuntos pendentes:

- Plano sobre o material a entregar dia 4.

---

#### Ata redigida por: Rui Bernardo
#### Ata revista por: Marta Santos, Pedro Mendes, Eva Simões
