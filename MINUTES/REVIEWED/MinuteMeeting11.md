# Ata de Reunião nº 11
---
Departamento de Engenharia Informática/Discord de Equipa, dia 17 de novembro de 2021

**Equipa:** GitNub

**Duração:** 15 minutos

**Presenças:**
- Carlos Jordão (Presencial)
- David Leitão (Presencial)
- Duarte Henriques (Presencial)
- Gonçalo Folhas (Presencial)
- João Fernandes (Presencial)
- João Vaz (Presencial)
- Pedro Mendes (Presencial)
- Rodrigo Machado (Presencial)
- Samuel Pires (Presencial)
- Adriana Bernardo (Discord)
- Eva Simões (Discord)
- Fransico Faria (Discord)
- Gonçalo Folhas (Discord)
- Guilherme Junqueira (Discord)
- Hugo Ribeiro (Discord)
- Iago Bebiano (Discord)
- Inês Galvão (Discord)
- Luís Santos (Discord) 
- Margarida Souto (Discord)
- Maria Raposo (Discord)
- Maria Sarmento (Discord)
- Marta Santos (Discord)
- Pedro Chaves (Discord) 
- Rui Bernardo (Discord)
- Tiago Ventura (Discord)

**Ausências**

---
## Sumário da reunião:
		Verificação do trabalho feito desde a última reunião.
		Explicação sobre a forma de submissão de issues.
		Atualização da ordem da cadeia de trabalho.
		Esclarecimento quanto às datas de entrega e explicação do trabalho a ser feito.
		Conversa com a equipa de Design sobre o que foi feito até ao momento.

---
## Assuntos tratados:
	Após a equipa de Arquitetura ter definido as views, vários issues foram alterados sendo que alguns destes continham pequenos erros, tais como o número do issue.
	Assim, ficou definido que se corrigiriam os números dos issues criados pelas equipas de Developing e de Design de forma a criar uma cadeia de trabalhos.
	Dado à impossibilidade das equipas de Design e Devoloping trabalharem em paralelo, foi definida uma nova ordem de trabalho sendo agora Requirements -> Architecture -> Design -> Developing -> Quality/Testing -> Integration.
	Até dia 4 teremos de ter preparado um pdf com a última versão do nosso trabalho e links da versão online e um link da lista de requisitos que a versão suporta.
	Até dia 18 continuaremos a fazer alterações, mas teremos também de trabalhar num pdf contendo os resultados dos testes da aplicação recebida.
	O próximo passo do projeto vai ser a equipa de Design a tratar por isso foi nos dado o ponto da situação acerca do que está a ser desenvolvido mostrando a interface que está a ser desenvolvida.



---
## Assuntos pendentes:
	Nenhum assunto ficou pendente no final desta reunião.

---

#### Ata redigida por: Eva Simões e Rui Bernardo
#### Ata revista por: Guilherme Junqueira, Marta Santos e Francisco Faria
