# Ata de Reunião nº 15
---
Discord, dia 01 de Dezembro de 2021

**Equipa:** GitNub

**Duração:** 30 minutos

**Presenças:**
- Adriana Bernardo
- Bernardo Arzileiro
- David Leitão
- Duarte Henriques
- Francisco Faria
- Gonçalo Folhas
- Guilherme Junqueira
- Hugo Ribeiro
- Iago Bebiano
- Inês Galvão
- João Fernandes
- João Vaz
- Maria Raposo
- Marta Santos
- Pedro Chaves
- Pedro Mendes
- Rodrigo Machado
- Rui Bernardo
- Tiago Ventura
- Samuel Pires
- Carlos Jordão

**Ausências:**
- Eva Simões
- Luís Santos
- Rodrigo Machado
- Margarida Souto
- Maria Sarmento

---
## Sumário da reunião:
Foco no que é suposto entregar até dia 4 de Dezembro.


---
## Assuntos tratados:

PMs  questionam e definem plano para a entrega de dia 4:
  1. Identificação do projecto que irá ser testado.
  2. ID da versão e respectiva data.
  3. Um link para essa versão online do produto.
  4. Um link para a lista de requisitos que essa versão do produto suporta. Este link deve apontar para um ficheiro .md no repositório Gitlab desse projecto.
  5. A lista de requisitos, por extenso, referida em 4 (i.e. o dump do ficheiro).

Discussão sobre vantagens e desvantagens de manter os issues abertos.

São dados alguns guidelines para potenciar o trabalho dos testers -> Não fazer código.


---

#### Ata redigida por: Iago Bebiano
#### Ata revista por: David Leitão, Gonçalo Folhas, Guilherme Junqueira, Rui Bernardo
