# Arquitetos

## Equipa de Arquitetos: 
- Pedro Mendes
- Bernardo Arzileiro 
- Hugo Ribeiro

## Processo:
- Criação a partir dos User Stories verificação de requisitos de produto;
- Organização de requisitos de produto em endpoints;
- Definição das *views* de modo a interligar com os vários requisitos.

## Organização da pasta:
Nesta pasta encontram-se os ficheiros referentes à equipa de arquitetura.
