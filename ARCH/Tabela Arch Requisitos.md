# GIT NUB - Documento de requisitos de Produto
---

**Estado**: Em aberto 

**Autor/es do documento**: Pedro Mendes [@Pedroooo], Hugo Ribeiro [@hrib], Bernardo Azileiro [@bennyaltf4]

|Issue Arch | Utilizador: | Sistema: | View: | Nome Funcionalidade:| Exceções: |
| --- | --- | --- | --- | --- | --- |
| #56 | Landing Page | |gitnub.com/| | |
| #80 | Utilizador abre o Gitlab, faz login e abre um repositório. | Sistema abre vista de repositório que tem uma opção "Diagrama de ficheiros". | gitnub.com/repository/tree|  | |
|  | Utilizador entra na opção "Diagrama de ficheiros". | Sistema mostra diagrama em árvore dos respetivos ficheiros e suas diretorias. |  |  | |
| #115 | Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros, tendo nela opções para ordenamento e seleção dos ficheiros da mesma| gitnub.com/repository/tree |tree | |
| | Utilizador seleciona o ficheiro | Sistema mostra o conteúdo do ficheiro do último commit feito. |gitnub.com/repository/tree/'file name'/last | last_commit|  ||
| #63 | Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros, tendo nela opções para ordenamento e seleção dos ficheiros da mesma |  gitnub.com/repository/tree | tree| |
|  | Utilizador seleciona a opção 'Mais alterados' | Sistema mostra a lista de ficheiros, desta vez ordenada por ordem, tendo os ficheiros com mais alterações primeiro, e os ficheiros com menos alterações em último. | | Parametro Opcional: ?order=most_changed | |
| #65  | Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros. |  gitnub.com/repository/tree | | |
|  | Utilizador seleciona a opção 'Agrupar por Autor' | Sistema mostra a lista de ficheiros agrupados por autor. | | | |
| #116 | Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros, tendo nela opções para ordenamento e seleção dos ficheiros da mesma.| gitnub.com/repository/tree |tree||
| | Utilizador seleciona a categoria desejada | Sistema mostra a lista de ficheiros da categoria correspondente. | gitnub.com/repository/tree/'category'| category||
| #64 | Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros, exibindo a opção de seleção deles mesmos. | gitnub.com/repository/tree |  | |
|  | Utilizador seleciona a opção 'Histórico' | Sistema mostra o histórico do ficheiro selecionado, estando ainda na lista de ficheiros|  | | |
|#120| Utilizador carrega na lista de membros ativos no repositório | Sistema mostra a página com a lista de de membros ativos.|gitnub.com/repository/members| members||
| #74 | Utilizador vai à lista de membros ativos | Sistema abre a lista de membros, exibindo a opção de seleção deles mesmos. |gitnub.com/repository/members | members  | |
|  | Utilizador seleciona o perfil do membro | Sistema mostra a página de perfil do membro selecionado. |gitnub.com/repository/members/'member'  |  | member |
| #72 | Utilizador vai à lista de membros ativos | Sistema abre a lista de membros, exibindo a opção de seleção deles mesmos.| gitnub.com/repository/members|members||
||Utilizador clica num dos membros da lista | Sistema abre o perfil do membro, exibindo as suas informações, incluindo a opção de 'commits' |gitnub.com/repository/members/'member name'|member||
|| Utilizador seleciona a opção 'commits' | Sistema apresenta a lista de commits do membro selecionado, sendo possível visualizar o histórico de todos os commits.|gitnub.com/repository/members/'member name'/commits|member_commit||
| #66 | Utilizador seleciona, no menu, a opção 'Project information' | Sistema apresenta opções de seleção. | gitnub.com/repository/info |  | |
|  | Utilizador seleciona a opção 'Members' | Sistema abre a lista de membros do projeto. | gitnub.com/repository/members |  | |
|  | Utilizador seleciona um membro | Sistema exibe as contribuições desse membro, organizadas por categoria, bem como o role predominante do mesmo. | gitnub.com/repository/members/'member name' |  | |
|#121| Utilizador abre a análise estatística de esforço do projeto a partir do menu | Sistema mostra o gráfico de esforço cumulativo do projeto, organizado por categoria | gitnub.com/repository/effort_chart| effort_chart||
| #110 | Utilizador carrega na dashboard desejada | Sistema abre a dashboard selecionada, demonstrando as várias informações, incluindo a data da última atualização. |gitnub.com/repository/dashboard|dashboard||
| #67 | Utilizador vai à lista de issues ao longo do tempo | Sistema abre a lista de issues, tendo nela opções de issues abertos, ativos e total. | gitnub.com/repository/issues | issues | |
|  | Utilizador seleciona a opção "abertos" | Sistema mostra a lista de issues abertos, ao longo do tempo. | gitnub.com/repository/issues/open | open_issues  | |
| #73 | Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros |  |  | |
|  | Utilizador carrega em um ficheiro | Sistema apresenta o ficheiro, bem como uma lista de issues/tickets ativos associados ao ficheiro | gitnub.com/repository/tree/file/issues/ |  | |
| #79 | Utilizador vai à lista de ficheiros. | Sistema abre a lista de ficheiros. |gitnub.com/repository/tree/  |  | |
|  | Utilizador seleciona ficheiro e verifica detalhes. | Sistema mostra a timeline de issues associados ao ficheiro, dando a possibilidade de aceder a cada um dos issues. |gitnub.com/repository/tree/'file name'  |file | |
| #122| Utilizador abre as contribuições feitas no repositório | Sistema mostra a lista de contribuições dos membros ativos do repositório. ||||
| #71 | Utilizador vai à lista de commits | Sistema abre a lista de commits. | gitnub.com/repository/commits | Parametro Opcional: <br> ?order=timeline | |
|  | Utilizador seleciona a opção 'visualização em linha temporal' | Sistema apresenta a lista de commits, em linha temporal uniforme. |  |  | |
| #81 | Utilizador seleciona, no menu, a opção 'Repository' | Sistema abre a lista de opções de seleção. | gitnub.com/repository/ |  | |
|  | Utilizador seleciona a opção 'Branches' | Sistema exibe a lista de branches. | gitnub.com/repository/branches/ |  | |
|  | Utilizador seleciona um branch | Sistema apresenta os dados relativos ao branch selecionado. | gitnub.com/repository/branches/'branch name' |  | |
| #75 | Utilizador abre uma vista e em seguida abre outra vista. | Sistema abre a vista selecionada pelo utilizador. |  | | Caso não haja nenhuma página anterior, a vista aberta quando se pressiona o botão deve ser a mesma em que o utilizador já estava |
|  | Utilizador seleciona um botão de "back" | Sistema devolve a vista anteriormente visitada pelo utilizador. |  | return | |
| #70 | Utilizador seleciona um repositório | Sistema abre o repositório selecionado. | gitnub.com/repository  | repo | Temos que verificar se o utilizador tem acesso a esse repositório |
|  | Utilizador tenta aceder ao campo de informação interna de um repositório | Sistema permite o acesso se o utilizador for autenticado, e rejeita o acesso caso contrário. | gitnub.com/repository/info |  | |
| #69 | Utilizador vai à lista de repositórios | Sistema abre a lista de repositórios, exibindo a opção de seleção deles mesmos. | | | |
|  | Utilizador seleciona um repositório à escolha | Sistema apresenta o conteúdo do repositório selecionado. |  gitnub.com/repository | | repo | Temos que verificar se o utilizador tem acesso a esse repositório |
| #68 | Utilizador vai ao menu | Sistema abre o menu, exibindo varias opções inclusive a mudança de idioma. | | | |
|  | Utilizador seleciona uma opção de idioma | Sistema apresenta o dashboard no idioma selecionado. | | Parametro Opcional: <br> ?lang='escolhida' | |
| #77 | Utilizador vai à lista de commits | Sistema abre a lista de commits, exibindo a opção de seleção deles mesmos. |gitnub.com/repository/commits | commits | |
|  | Utilizador seleciona um commit | Sistema abre o commit, exibindo a sua lista de ficheiros. |  |  | |
|  | Utilizador seleciona 'Historico' | Sistema mostra a lista do histórico de ficheiros alterados e por qual membro. |  |  | |


