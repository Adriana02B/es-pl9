## Tutorial:
Para passar de Requisitos (User Stories) a Requisitos de Produto de forma à equipa de Dev perceber melhor, devemos explicar o que o sistema faz e responde a certa ação de um utilizador.

Para isso usaremos esta tabela: 
O objetivo será dividir as várias etapas de certo requisito e a resposta do sistema a tal etapa, como se vê na tabela seguinte.

Copiem o template e usem nos vossos issues para termos uniformidade.

Usem a label "Architecture" e não se esqueçam de se meter como Assignees.

--------
### Issue: #38

--------
|Utilizador Faz: | Sistema Faz: |
| --- | --- |
| Utilizador vai à lista de ficheiros | Sistema abre a lista de ficheiros, tendo nela opções para ordenamento e seleção dos ficheiros da mesma|
| Utilizador seleciona a opção 'Mais alterados' | Sistema mostra a lista de ficheiros, desta vez ordenada por ordem, tendo os ficheiros com mais alterações primeiro, e os ficheiros com menos alterações em último. |
