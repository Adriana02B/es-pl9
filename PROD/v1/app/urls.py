from django.urls import path, re_path
from . import views

app_name = 'app'

urlpatterns = [
    path('', views.index, name='index'),
    path('members', views.listMembers, name='members'),
    re_path(r'^repository/(?P<full_slug>(.*))', views.repository, name='files'),
    path('member/<str:username>/commits', views.listMemberCommits, name='memberCommits'),
    path('commits', views.commitList, name='commits'),
    path('issues', views.listAllIssues, name='issues'),
    path('issues/open', views.listOpenIssues, name='openIssues'),
    path('issues/closed', views.listClosedIssues, name='closedIssues'),
]