from django.http.request import HttpRequest
from django.shortcuts import redirect, render, HttpResponse
from pydriller import Repository
from git.repo import Repo
import logging

from app.models import ProjectMember
from . import gitlab
from .utils import getRepoPath, getRepository
from .forms import Picker

logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# route: Waiting for ARCH's final say
def listMembers(request: HttpRequest) -> HttpResponse:
    # members = gitlab.getMembers()
    members = ProjectMember.objects.order_by('name')
    if members is None: return render(request, 'app/error.html', {'error': 'Algo correu mal'})
    # members.sort(key=lambda x: x.name.lower())
    return render(request, 'app/members.html', {'members': members})
    
def listMemberCommits(request: HttpRequest, username: str) -> HttpResponse:
    orderType = request.GET.get('order', None)
    branch = request.GET.get('branch') or 'main'
    order = "date_order" if orderType == 'oldest' else 'reverse'

    git = Repo(getRepoPath(gitlab.REPO_URL))
    orderSet = [('newest', 'Newest to Oldest'), ('oldest', 'Oldest to Newest')]
    if orderType == 'oldest': orderSet.reverse()
    branchesSet = [branch,]
    for ref in git.heads:
        if str(ref) not in branchesSet: branchesSet.append(str(ref))
    form = Picker(None, branchesSet) 
    
    commits = []
    try:
        repo = Repository(getRepoPath(gitlab.REPO_URL), only_in_branch=branch, order=order)
    except:
        return render(request, 'app/error.html', {'error': 'Branch does not exist!'})

    dbquery = ProjectMember.objects.filter(username=username)
    email: str
    name: str
    if dbquery.exists():
        email = dbquery[0].email
        name = dbquery[0].name
    else: 
        return render(request, 'app/error.html', {'message': 'Failed to retrieve user\'s email!'})

    for commit in repo.traverse_commits():
        if commit.author.email == email: commits.append(commit.msg)
    
    context = {
        'name': name,
        'commits': commits,
        'form': form
    }
    return render(request, 'app/list.html', context)

def repository(request, full_slug):
    #serve para no futuro existir possibilidade de abrirmos mais repositórios
    text = "Repository"
    owner = "Adriana02B"
    repo = "es-pl9"

    data, htmlFile, text = getRepository(full_slug, owner, repo)

    return render(request, htmlFile, {'text': text, 'data': data})

def commitList(request):
    commit = []
    for element in Repository(getRepoPath(gitlab.REPO_URL), order = 'reverse').traverse_commits():
        commit.append({
            'msg': element.msg,
            'author': element.committer.name,
            'date': element.committer_date
        })
    return render(request, "app/commit.html", {'text': "Commit List", 'data': commit})

def listAllIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues()
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)
    
    context = {
        'text': 'All Issues',
        'issues': issues
    }
    return render(request, 'app/issues.html', context)

def listOpenIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues(state='opened')
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)
    
    context = {
        'text': 'Open Issues',
        'issues': issues
    }
    return render(request, 'app/issues.html', context)

def listClosedIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues(state='closed')
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)

    context = {
        'text': 'Closed Issues',
        'issues': issues
    }
    return render(request, 'app/issues.html', context)