from django.contrib import admin
from .models import LocalRepo, ProjectMember

# Register your models here.
@admin.register(LocalRepo)
class LocalRepoAdmin(admin.ModelAdmin):
    list_display = ('path', 'url', 'timestamp')

@admin.register(ProjectMember)
class ProjectMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'role')
    search_fields = ['username', 'name']
    list_filter = ('role',)