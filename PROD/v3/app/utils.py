import markdown
import os
import logging
import shutil
from git import Repo
from pydriller import Repository
from git.exc import GitCommandError
from datetime import timedelta
from django.utils import timezone

from . import gitlab
from .models import LocalRepo

logger = logging.getLogger(__name__)

def clone(url: str) -> str:
    split = url.split('/')
    owner = split[-2]
    repo = split[-1]
    path = f"git/{owner}/{repo}"
    try:
        repo = Repo.clone_from(url, path)
        repo.remotes.origin.fetch()
        entry = LocalRepo(url=url, path=path)
        entry.save()
    except GitCommandError:
        logger.error(f"Clone {url} failed!")
        try:
            shutil.rmtree(f"git/{owner}/{repo}")
        except:
            logger.error("Failed to remove folder")
        return None
    return f"git/{owner}/{repo}"

def updateBranches(path: str):
    repo = Repo(path)
    localRefs = repo.heads
    for branch in repo.remotes.origin.refs:
        branchName = branch.name.split('/')[1]
        if branch.name == 'origin/HEAD' or branchName in localRefs: 
            logger.info(f'Branch {branch.name} already on local refs')
            continue
        repo.git.checkout('-b', branchName, branch.name)
        logger.info(f'Checked out remote branch {branch.name}')

def update(url: str):
    query = LocalRepo.objects.filter(url=url)
    if not query.exists(): return

    entry: LocalRepo = query[0]
    repo = Repo(entry.path)
    try:
        repo.remotes.origin.pull()
        logger.info(f"Updated repo {url}")
        updateBranches(entry.path)
        entry.save()
    except:
        logger.error(f'Failed to pull from remote {url}')

def getRepoPath(url: str):
    query = LocalRepo.objects.filter(url=url)
    if query.exists():
        entry: LocalRepo = query[0]
        logger.info("Git repo already exists on local machine")
        path = entry.path
        if entry.timestamp + timedelta(hours=12) < timezone.now():
            update(entry.url)
    else:
        path = clone(url)
        if path is None:
            return None
        logger.info("Git repo cloned to the local machine")
    return path
        

def checkoutBranch(path: str, branch: str = None) -> Repo:
    repo = Repo(path)
    if branch is not None and branch not in repo.branches:
        repo.git.checkout('-b', branch, f'origin/{branch}')
        logger.info(f'Remote branch {branch} checked out')
    else: logger.info('Branch already in local refs')

####### Req. 104 #######

class File:
    def __init__(self, url, name):
        self.url = url
        self.name = name

    def __repr__(self):
        return self.url

#verifica se o ficheiro ou pasta existe
def verifyFileOrPath(path, lastItem):
    for i in os.listdir(path):
        if(lastItem == i):
            return True
    return False

#abre o ficheiro
def openFile(path, lastItem, folder):
    htmlFile = "app/file.html"

    try:
        fileTxt = open(path, "r", encoding="utf-8")
        text = fileTxt.read()

        #verifica se e markdown
        if(lastItem.split(".")[1] == "md"):
            htmlReturn = markdown.markdown(text, extensions=['tables', 'extra', 'abbr', 'attr_list', 'def_list', 'fenced_code', 'footnotes', 'md_in_html', 'admonition', 'codehilite', 'legacy_attrs', 'legacy_em', 'meta', 'nl2br', 'sane_lists', 'smarty', 'toc', 'wikilinks'])
        elif(lastItem.split(".")[1] == "html"):
            htmlFile = "app/filesNoHtml.html"
            htmlReturn = text
        else:
            #prepara o ficheiro para html
            text = text.replace('\n','<br>')
            text = text.replace('\t','&#9')
            text = "<pre>" + text + "</pre>"
            htmlReturn = text

    except:
        htmlReturn = "<H4 style= 'color:Red'>"+ folder +" Impossivel Abrir Formato de Ficheiro</H4>" #mensagem de erro

    title = folder[1:]
    return htmlReturn, htmlFile, title

#retorna os ficheiros da pasta do repositorio
def getFilesRepo(auxPath, folder):
    htmlFile = "app/repository.html"
    folders = []
    files = []

    #vai buscar as pastas e os ficheiros
    for i in os.listdir(auxPath):
        if(i != ".git"):
            if folder: file = File(f"{folder}/{i}", i)
            else: file = File(i, i)
            #ordena 1 pastas, 2 ficheiros
            if(os.path.isfile(i) == False):
                folders.append(file)
            else:
                files.append(file)

    #junta ficheiros a pastas
    folders = folders + files
    return folders, htmlFile, folder

#retorna o ficheiro ou as pastas
def getRepository(folder, owner, repo):
    #get repo
    path = getRepoPath(gitlab.REPO_URL)
    
    #variaveis necessarias
    try:
        if(folder[-1] == "/"):
            folder = folder.strip("/")
    except:
        pass
    lastItem = folder.split("/")[-1]
    pathAux = "./" + path + '/' + folder.replace(lastItem,'')
    path = "./" + path + '/' + folder

    #verifica se ficheiro ou pasta existe
    if(verifyFileOrPath(pathAux, lastItem) == False and path != pathAux):
        htmlFile = "app/files.html"
        title = lastItem + " não existe"
        htmlReturn = "<H4 style= 'color:Red'>" + lastItem + " não existe </H4>" #mensagem de erro
        return htmlReturn, htmlFile, title,

    #verifica se o pedido e um ficheiro ou uma pasta
    if(os.path.isfile(path) ==  True):
        return openFile(path, lastItem, folder)
    else:
        return getFilesRepo(path, folder)