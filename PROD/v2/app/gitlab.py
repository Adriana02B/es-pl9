import logging
import requests
from typing import List
from datetime import datetime
from .models import ProjectMember

logger = logging.getLogger(__name__)
REPO_URL = "https://gitlab.com/Adriana02B/es-pl9"
PRIVATE_TOKEN = "glpat-SNyaPe5sK6Lx3316WiWr"
REPO_ID = "30034710"
API_URL = f"https://gitlab.com/api/v4/projects/{REPO_ID}"
HEADERS = {"PRIVATE-TOKEN": PRIVATE_TOKEN}

class Member:
    name: str
    username: str
    email: str
    role: str
    css: str

    def __init__(self, name: str, username: str, email: str, role: str = "No Role", css: str = None):
        self.name = name
        self.username = username
        self.email = email
        self.role = role
        self.css = css
    
    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

def getMembers() -> List[Member]:
    URL = f"{API_URL}/members/?per_page=100"
    data = []
    members = []
    i = 1
    while True:
        try:
            req = requests.get(
                f"{URL}&page={i}",
                headers=HEADERS
            )
            json = req.json()
            if json != []: data += json
            else: break
            i += 1
        except:
            return None
    for object in data:
        name = object.get('name')
        username = object.get('username')
        query = ProjectMember.objects.filter(username=username)
        if query.exists():
            pmember: ProjectMember = query[0]
            email = pmember.email
            role = pmember.role
            css = pmember.css
            members.append(Member(name, username, email, role, css))
        else: members.append(Member(name, username, email))
            
    return members

class Issue:
    title: str
    id: int
    cd: datetime
    authorn: str
    state: str

    def __init__(self, title, id, cd, authorn, state):
        self.title = title
        self.id = id
        self.cd = cd
        self.authorn = authorn
        self.state = state

def getIssues(state: str = None) ->List[Issue]:
    i = 1
    URL = f"{API_URL}/issues/?per_page=100"
    if state is not None:
        URL = URL + f"&state={state}"
    data = []
    issues = []
    while True:
        try:
            req = requests.get(
                f"{URL}&page={i}",
                headers=HEADERS
            )
            json = req.json()
            i += 1
            if json != []: data += json
            else: break
        except:
            logger.exception("Caught an exception")
            return None
    for object in data:
        issues.append(Issue(object.get("title"),object.get("id"),object.get("cd"),object.get("authorn"),object.get("state")))
    return issues

class Branch:
    title: str
    lupdater: str
    ldate: datetime

    def __init__(self, title, lupdater, ldate):
        self.title = title
        self.lupdater = lupdater
        self.ldate = ldate

def getBranches() -> List[Branch]:
    i = 1
    URL = f"{API_URL}/repository/branches/?per_page=100"
    data = []
    branch = []
    while True:
        try:
            req = requests.get(
                f"{URL}&page={i}",
                headers=HEADERS
            )
            json = req.json()  #hello father how are u
            i += 1
            if json != []: data += json
            else: break
        except:
            logger.exception("Caught an exception")
            return None
    for object in data:
        name = object.get('name')
        author = object.get('commit').get('author_name')
        date: str = object.get('commit').get('committed_date')
        date = date[:-10]+date[-6:-3]+date[-2:]
        date = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S%z")
        branch.append(Branch(name, author, date))
    return branch
    