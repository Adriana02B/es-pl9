from typing import Dict
from django import forms
from django.db.models.fields import BLANK_CHOICE_DASH

class Picker(forms.Form):
    order = forms.ChoiceField(
        required=False, choices=[], label='',
        widget=forms.Select(attrs={'onchange': 'submit();'})
    )

    branch = forms.ChoiceField(
        required=False, choices=[], label='',
        widget=forms.Select(attrs={'onchange': 'submit();'})
    )

    def __init__(self, order, choices, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if order is None: del self.fields['order']
        else:
            self.fields['order'].choices = [
                item for item in order
            ]
        if choices is None: del self.fields
        else: 
            self.fields['branch'].choices = [
                ref if type(ref) == tuple else (ref, ref) for ref in choices 
            ]