from django.contrib import admin
from django.db import models
from datetime import datetime
# Create your models here.

class LocalRepo(models.Model):
    url = models.CharField(max_length=255, unique=True)
    path = models.CharField(max_length=255, unique=True)
    timestamp = models.DateTimeField(auto_now=True, verbose_name='Last Updated')

class ProjectMember(models.Model):
    ROLES = [
        ('Quality Assurance', 'Quality Assurance'),
        ('Development', 'Development'),
        ('Project Management', 'Project Management'),
        ('Architecture', 'Architecture'),
        ('Tests', 'Tests'),
        ('Requirements', 'Requirements'),
        ('Design', 'Design')
    ]
    CSS = [
        ('qa', 'Quality Assurance'),
        ('dev', 'Development'),
        ('pm', 'Project Management'),
        ('arch', 'Architecture'),
        ('tes', 'Tests'),
        ('req', 'Requirements'),
        ('des', 'Design')
    ]
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255, unique=True)
    email = models.CharField(max_length=255, unique=True)
    role = models.CharField(max_length=255, choices=ROLES)
    css = models.CharField(max_length=255, choices=CSS, null=True)