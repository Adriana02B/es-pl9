from django.http.request import HttpRequest
from django.shortcuts import redirect, render, HttpResponse
from pydriller import Repository
from git.repo import Repo
import logging

from app.models import ProjectMember
from . import gitlab
from .utils import getRepoPath, getRepository
from .forms import Picker

logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: / 
def index(request):
    return render(request, 'app/index.html')

# Req. 101 - Rodrigo Machado
# Helper for listMembers and listActiveMembers
def viewMembers(request: HttpRequest, template: str) -> HttpResponse:
    members = gitlab.getMembers()
    if members is None: return render(request, 'app/error.html', {'error': 'Algo correu mal'})
    members.sort(key=lambda x: x.name.lower())
    return render(request, template, {'members': members})

# Req. 101 - Rodrigo Machado
# route: /members
def listMembers(request: HttpRequest) -> HttpResponse:
    return viewMembers(request, 'app/members.html')


# Req. 101 - Rodrigo Machado
# route: /active
def listActiveMembers(request: HttpRequest) -> HttpResponse:
    # TODO only return people with commits
    return viewMembers(request, 'app/activeMembers.html')
    
# Req. 102 (modified)
# route: /member/<username>/commits
def listMemberCommits(request: HttpRequest, username: str) -> HttpResponse:
    try:
        repo = Repository(getRepoPath(gitlab.REPO_URL))
    except:
        return render(request, 'app/error.html', {'error': 'Branch does not exist!'})

    dbquery = ProjectMember.objects.filter(username=username)
    if not dbquery.exists():
        return render(request, 'app/error.html', {'message': 'Failed to retrieve user\'s email!'})
    
    member: ProjectMember = dbquery[0]
    email = member.email

    commits = []
    for commit in repo.traverse_commits():
        if commit.author.email == email: 
            commits.append(commit)
    
    context = {
        'member': member,
        'commits': commits,
    }
    return render(request, 'app/memberCommits.html', context)

# Req. 104 - Samuel Pires
# route: /repository
def repository(request, full_slug):
    #serve para no futuro existir possibilidade de abrirmos mais repositórios
    text = "Repository"
    owner = "Adriana02B"
    repo = "es-pl9"

    data, htmlFile, text = getRepository(full_slug, owner, repo)

    return render(request, htmlFile, {'text': text, 'data': data})

# Req. 102 - Gonçalo Folhas
# route: /commits
def commitList(request):
    commit = []
    for element in Repository(getRepoPath(gitlab.REPO_URL), order = 'reverse').traverse_commits():
        query = ProjectMember.objects.filter(email=element.committer.email)
        css = None
        if query.exists(): css = query[0].css
        commit.append({
            'msg': element.msg,
            'author': element.committer.name,
            'date': element.committer_date,
            'css': css
        })
    return render(request, "app/commits.html", {'commits': commit})

# Req. 135 - David Leitão
# route: /issues
def listAllIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues()
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)
    
    context = {
        'text': 'All Issues',
        'issues': issues
    }
    return render(request, 'app/issues.html', context)

# Req. 135 - David Leitão
# route: /issues/open
def listOpenIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues(state='opened')
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)
    
    context = {
        'text': 'Open Issues',
        'issues': issues
    }
    return render(request, 'app/issues.html', context)
    
# Req. 135 - David Leitão
# route: /issues/closed
def listClosedIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues(state='closed')
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)

    context = {
        'text': 'Closed Issues',
        'issues': issues
    }
    return render(request, 'app/issues.html', context)

# Req. 162 - David Leitão
# route: /branches
def branches(request: HttpRequest) -> HttpResponse:
    branches = gitlab.getBranches()
    if branches is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    return render(request, 'app/branches.html', {'branches': branches})