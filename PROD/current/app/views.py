from django.http.request import HttpRequest
from django.shortcuts import redirect, render, HttpResponse
from pydriller import Repository, Git
from pydriller.metrics.process.commits_count import CommitsCount
from git.repo import Repo
import logging

from app.models import ProjectMember
from . import gitlab
from .utils import getRepoPath, getRepository, getFilePaths
from .forms import Picker

logger = logging.getLogger(__name__)
# Create your views here.
# Don't forget to route to your views in app/urls.py!

# route: /
def index(request):
    return render(request, 'app/index.html')

# Req. 101 - Rodrigo Machado
# Helper for listMembers and listActiveMembers
def viewMembers(request: HttpRequest, template: str) -> HttpResponse:
    members = gitlab.getMembers()
    if members is None: return render(request, 'app/error.html', {'error': 'Algo correu mal'})
    members.sort(key=lambda x: x.name.lower())
    return render(request, template, {'members': members})

# Req. 101 - Rodrigo Machado
# route: /members
def listMembers(request: HttpRequest) -> HttpResponse:
    return viewMembers(request, 'app/members.html')


# Req. 101 - Rodrigo Machado
# route: /active
def listActiveMembers(request: HttpRequest) -> HttpResponse:
    # TODO only return people with commits
    return viewMembers(request, 'app/activeMembers.html')

# Req. 102 (modified)
# route: /member/<username>/commits
def listMemberCommits(request: HttpRequest, username: str) -> HttpResponse:
    try:
        repo = Repository(getRepoPath(gitlab.REPO_URL))
    except:
        return render(request, 'app/error.html', {'error': 'Branch does not exist!'})

    dbquery = ProjectMember.objects.filter(username=username)
    if not dbquery.exists():
        return render(request, 'app/error.html', {'message': 'Failed to retrieve user\'s email!'})

    member: ProjectMember = dbquery[0]
    email = member.email

    commits = []
    for commit in repo.traverse_commits():
        if commit.author.email == email:
            commits.append(commit)

    context = {
        'member': member,
        'commits': commits,
    }
    return render(request, 'app/memberCommits.html', context)

# Req. 104 - Samuel Pires
# route: /repository
def repository(request, full_slug):
    #serve para no futuro existir possibilidade de abrirmos mais repositórios
    text = "Repository"
    owner = "Adriana02B"
    repo = "es-pl9"

    data, htmlFile, text = getRepository(full_slug, owner, repo)

    filename = ""
    paths = []
    md = False

    if full_slug:
        part = full_slug.split('/')
        if part[0] == '': part = part[1:]

        path = [x for x in part[:-1]]
        redirects = []
        for i in range(1, len(part)):
            redirect = ""
            for j in range(i):
                redirect += part[j]
                if j != i: redirect += '/'
            redirects.append(redirect)
        paths = zip(path, redirects)

        filename = part[-1]
        extension = filename.split('.')[-1]
        print(extension)
        if extension == 'md': md = True

    context = {
        'text': text,
        'data': data,
        'filename': filename,
        'paths': paths,
        'markdown': md,
        'slug': full_slug
    }
    return render(request, htmlFile, context)

# Req. 102 - Gonçalo Folhas
# route: /commits
def commitList(request):
    commit = []
    for element in Repository(getRepoPath(gitlab.REPO_URL), order = 'reverse').traverse_commits():
        query = ProjectMember.objects.filter(email=element.committer.email)
        css = None
        if query.exists(): css = query[0].css
        commit.append({
            'msg': element.msg,
            'author': element.committer.name,
            'date': element.committer_date,
            'css': css
        })
    return render(request, "app/commits.html", {'commits': commit})

# Req. 135 - David Leitão
# route: /issues
def listAllIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues()
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)

    context = {
        'text': 'All Issues',
        'issues': issues
    }
    return render(request, 'app/allIssues.html', context)

# Req. 135 - David Leitão
# route: /issues/open
def listOpenIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues(state='opened')
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)

    context = {
        'text': 'Open Issues',
        'issues': issues
    }
    return render(request, 'app/openIssues.html', context)

# Req. 135 - David Leitão
# route: /issues/closed
def listClosedIssues(request: HttpRequest) -> HttpResponse:
    issues = gitlab.getIssues(state='closed')
    if issues is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    issues.sort(key=lambda x: x.id, reverse=True)

    context = {
        'text': 'Closed Issues',
        'issues': issues
    }
    return render(request, 'app/closedIssues.html', context)

# Req. 162 - David Leitão
# route: /branches
def branches(request: HttpRequest) -> HttpResponse:
    branches = gitlab.getBranches()
    if branches is None: return render(request, 'app/error.html', {'message': 'Algo correu mal'})
    return render(request, 'app/branches.html', {'branches': branches})

# Req. 112 - João Fernandes
# route: /most_changed
def mostChanged(request):
    git = getRepoPath(gitlab.REPO_URL)
    gr = Git(git)

    #Buscar os ficheiros e o numero de alterações - guarda dicionario files
    #Devolve alguns ficheiros nao existentes
    first_commit = None
    for commit in Repository(git, only_in_branch= "main").traverse_commits():
        first_commit = commit.hash
        break
    last_commit = None
    for commit in Repository(git, only_in_branch= "main", order= 'reverse').traverse_commits():
        last_commit = commit.hash
        break
    metric = CommitsCount(path_to_repo=git,
                        from_commit=first_commit,
                        to_commit=last_commit)
    files = metric.count()
    #Organiza o dicionario para ficarem os mais alterados primeiros
    final_dic = {}
    for i in sorted(files, key = files.get, reverse=True):
        final_dic[i] = files[i]

    #Buscar os ficheiros que existem e guarda na lista paths
    paths = []
    for path in gr.files():
        path_split = path.split('/')
        index = path_split.index("Adriana02B")

        paths.append(('/'.join(path_split[index+2:])))
    #Adiciona os ficheiros ja organizados à lista.Não adiciona os ficheiros que não existem
    files_organized = []
    for key in final_dic.keys():
        if key is not None:
            if key in paths:
                files_organized.append(key)

    return render(request, "app/filesList.html", {'files': files_organized})

# Req. 127 - Gonçalo Folhas
# route:
def modList(request: HttpRequest, path):
    files =  []
    repo = Repo(getRepoPath(gitlab.REPO_URL))
    print(path)
    paths = [path, path.replace('/', '\\')]
    name = path.split('/')[-1]

    # commits_touching_path = list(map(str, repo.iter_commits(paths=path)))
    # print(commits_touching_path)
    commits_touching_path = getFilePaths(repo, paths)
    #print(commits_touching_path)
    drill = Repository(getRepoPath(gitlab.REPO_URL), order='reverse', only_commits=commits_touching_path)
    for commit in drill.traverse_commits():
        for m in commit.modified_files:
            if m.new_path in paths or m.old_path in paths:
                if m.change_type.name == "MODIFY":
                    type = "modified"
                elif m.change_type.name == "ADD":
                    type = "added"
                elif m.change_type.name == "DELETE":
                    type = "deleted"
                elif m.change_type.name == "COPY":
                    type = "copied"
                elif m.change_type.name == "RENAME":
                    type = "renamed"

                files.append({
                    'file': m.filename,
                    'author': commit.author.name,
                    'date': commit.author_date,
                    'type': type,
                    'add': m.added_lines,
                    'del': m.deleted_lines
                })
    return render(request, "app/history.html", {'text': f"Modifications for file {name}", 'data': files})
